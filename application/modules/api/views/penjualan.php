<style>
  table,
  th,
  td {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
  }
</style>

<table class="table" border="1">
  <tr>
    <td align="center" colspan="4">Laporan Penjualan Tanggal <?= $penjualan['data']->tanggal ?></td>
  </tr>
  <tr>
    <td align="left" style='font-weight:bold'>Nama Produk</td>
    <td align="center" style='font-weight:bold'>Quantity</td>
    <td align="center" style='font-weight:bold'>Harga Satuan</td>
    <td align="center" style='font-weight:bold'>Total</td>
  </tr>

  <?php
  foreach ($penjualan['penjualan_dtl'] as $key => $value) {
    // $skill[$value['id_skill_matrix']] = $value['nilai'];
    echo "<tr><td>" . $value['name'] . "</td><td align='right'>" . number_format($value['qty'], 0, ',', '.') . "</td><td align='right'>Rp " . number_format($value['price'], 0, ',', '.') . "</td><td align='right'>Rp " . number_format($value['price'] * $value['qty'], 0, ',', '.') . "</td></tr>";
  }
  ?>

</table>

<pre><?php 
// print_r($penjualan); 
      ?></pre>
<pre><?php
      // print_r($datas);


      ?></pre>