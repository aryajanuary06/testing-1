<?php

require APPPATH . 'libraries/REST_Controller.php';

class Users extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Users_model');
    $this->load->helper('common');
  }

  public function registration_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Users_model->registerUser($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function resetpassword_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Users_model->resetPassword($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function login_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Users_model->userLogin($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function logout_post()
  {
    $response = [
      "status" => "success",
      "message" => "Logout success"
    ];
    $responseCode = 202;
    $this->response($response, $responseCode);
  }

  public function getUserById_get($user_id)
  {
    $responseCode = '';
    $response = $this->Users_model->getUserById($user_id, $responseCode);
    $this->response($response);
  }

  public function userbyid_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Users_model->userById($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function editprofile_post()
  {
    $responseCode='';
    // $rawdata = file_get_contents("php://input");   
    // $data = json_decode($rawdata);

    $data_img = $_FILES["photo"];
    if ($data_img["size"] == 0 && $this->input->post('isUpload') == 'true' ) {
      $this->response([
        "status" => "error",
        "message" => "Error uploading the file!"
      ], 400);
    }

    $data_img2 = $_FILES["photo2"];
    if ($data_img2["size"] == 0 && $this->input->post('isUpload2') == 'true' ) {
      $this->response([
        "status" => "error",
        "message" => "Error uploading the file!"
      ], 400);
    }

    $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
    $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
    $data_img['name'] = $data_img['name'];
    $data_img['filename'] = $encrypted_gambar;
    $data_img['urlgambar'] = base_url("/assets/images/img_profile/$encrypted_gambar");

    $ext2 = pathinfo($data_img2['name'], PATHINFO_EXTENSION);
    $encrypted_gambar2 = $data_img2['name'] . date("Y-m-d H-i-s") . "." . $ext;
    $data_img2['name'] = $data_img2['name'];
    $data_img2['filename'] = $encrypted_gambar2;
    $data_img2['urlgambar'] = base_url("/assets/images/img_umkm_pp/$encrypted_gambar2");

    $response = $this->Users_model->editProfile($data_img, $data_img2, $responseCode);
    $this->response($response, $responseCode);
  }

  // public function editprofile_post()
  // {
  //   $responseCode = '';
  //   $rawdata = file_get_contents("php://input");
  //   $data = json_decode($rawdata);
  //   $response = $this->Users_model->editProfile($data, $responseCode);
  //   $this->response($response, $responseCode);
  // }

  // public function uploadphoto_post($user_id)
  // {
  //   $responseCode = '';
  //   $data = $_FILES["photo"];
  //   $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
  // $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
  //   $data['name'] = $data['name'];
  //   $data['filename'] = $encrypted_gambar;
  //   $data['urlgambar'] = base_url("/photos/$encrypted_gambar");
  //   // $this->response($data, $responseCode);

  //   $response = $this->Users_model->updatePP($user_id, $data, $responseCode);
  //   $this->response($response, $responseCode);
  // }

  // public function reset_password_post()
  // {
  //   $responseCode = '';
  //   $rawdata = file_get_contents("php://input");
  //   $data = json_decode($rawdata);
  //   $response = $this->Users_model->userLogin($data, $responseCode);
  //   $this->response($response, $responseCode);
  // }
}
