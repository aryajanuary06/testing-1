<?php

require APPPATH . 'libraries/REST_Controller.php';

class Kapasitas_prod extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Kapasitas_prod_model');
    $this->load->helper('common');
  }

  public function listKapasitas_prod_get($user_id)
  {
    $responseCode = '';
    $response = $this->Kapasitas_prod_model->getListKapasitas_prod($user_id, $responseCode);
    $this->response($response);
  }

  public function kapasitas_prodById_get($id_kapasitas_prod)
  {
    $responseCode = '';
    $response = $this->Kapasitas_prod_model->getKapasitas_prodById($id_kapasitas_prod, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Kapasitas_prod_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createKapasitas_prod_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Kapasitas_prod_model->createKapasitas_prod($data, $responseCode);
    $this->response($response, $responseCode);
  }
}
