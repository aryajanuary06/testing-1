<?php

require APPPATH . 'libraries/REST_Controller.php';

class BB extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('BB_model');
    $this->load->helper('common');
  }

  public function listBB_get($user_id)
  {
    $responseCode = '';
    $response = $this->BB_model->getListBB($user_id, $responseCode);
    $this->response($response);
  }

  public function BBById_get($id_BB)
  {
    $responseCode = '';
    $response = $this->BB_model->getBBById($id_BB, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->BB_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createBB_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->BB_model->createBB($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function katBB_get()
  {
    $responseCode = '';
    $response = $this->BB_model->getKatBB($responseCode);
    $this->response($response);
  }

  public function satuanBB_get()
  {
    $responseCode = '';
    $response = $this->BB_model->getSatuanBB($responseCode);
    $this->response($response);
  }

}
