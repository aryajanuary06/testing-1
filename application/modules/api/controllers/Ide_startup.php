<?php

require APPPATH . 'libraries/REST_Controller.php';

class Ide_startup extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Ide_startup_model');
    $this->load->helper('common');
  }

  public function listIde_startup_get()
  {
    $responseCode = '';
    $response = $this->Ide_startup_model->getListIde_startup($responseCode);
    $this->response($response);
  }

  public function ide_startupById_get($id_ide_startup)
  {
    $responseCode = '';
    $response = $this->Ide_startup_model->getIde_startupById($id_ide_startup, $responseCode);
    $this->response($response);
  }

  public function createIde_startup_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Ide_startup_model->createIde_startup($data, $responseCode);
    $this->response($response, $responseCode);
  }
}
