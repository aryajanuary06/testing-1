<?php

require APPPATH . 'libraries/REST_Controller.php';

class Media extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Media_model');
    $this->load->helper('common');
  }

  public function home_get($menu)
  {
    $responseCode = '';
    $response = $this->Media_model->getListMedia($menu, $responseCode);
    $this->response($response);
  }

  public function artikel_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListArtikel($responseCode);
    $this->response($response);
  }

  public function video_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListVideo($responseCode);
    $this->response($response);
  }

  public function inspirasi_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListInspirasi($responseCode);
    $this->response($response);
  }

  public function m_artikel_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListMArtikel($responseCode);
    $this->response($response);
  }

  public function m_video_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListMVideo($responseCode);
    $this->response($response);
  }

  public function m_ebook_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListMEbook($responseCode);
    $this->response($response);
  }

  public function m_webinar_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getListMWebinar($responseCode);
    $this->response($response);
  }

  public function knowledge_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Media_model->getListKnowledge($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function regulasi_get($id_kat)
  {
    $responseCode = '';
    $response = $this->Media_model->getListRegulasi($id_kat, $responseCode);
    $this->response($response);
  }

  public function commentById_get($id_posts)
  {
    $responseCode = '';
    $response = $this->Media_model->getCommentById($id_posts, $responseCode);
    $this->response($response);
  }

  public function createComment_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Media_model->createComment($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function artikelById_get($id_posts)
  {
    $responseCode = '';
    $response = $this->Media_model->getArtikelById($id_posts, $responseCode);
    $this->response($response);
  }

  public function log_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Media_model->log($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function anggota_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getAnggota($responseCode);
    $this->response($response);
  }

  public function kat_dewan_get()
  {
    $responseCode = '';
    $response = $this->Media_model->getKatDewan($responseCode);
    $this->response($response);
  }

}
