<?php

require APPPATH . 'libraries/REST_Controller.php';

class Pegawai extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Pegawai_model');
    $this->load->helper('common');
  }

  public function pegawaiInputMatrix_get($role_id, $id_spv = "")
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getPegawaiInputMatrix($role_id, $id_spv, $responseCode);
    $this->response($response);
  }
  
  public function danruByClient_get($user_id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getDanruByClient($user_id, $responseCode);
    $this->response($response);
  }

  public function anggota_get($id_spv)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getAnggota($id_spv, $responseCode);
    $this->response($response);
  }

  public function pegawaiApproveMatrix_get($role_id, $id_spv = 0)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getPegawaiApproveMatrix($role_id, $id_spv, $responseCode);
    $this->response($response);
  }

  public function historyApproveMatrix_get($role_id, $id_spv = 0)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getHistoryApproveMatrix($role_id, $id_spv, $responseCode);
    $this->response($response);
  }

  public function detailMatrixById_get($id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getDetailMatrixById($id, $responseCode);
    $this->response($response);
  }

  public function getPegawaiById_get($id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getPegawaiById($id, $responseCode);
    $this->response($response);
  }

  public function employeeMatrix_get($id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getEmployeeMatrixById($id, $responseCode);
    $this->response($response);
  }

  public function createEmployeeMatrix_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Pegawai_model->createEmployeeMatrix($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createEmployeePromosi_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Pegawai_model->createEmployeePromosi($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createEmployeeMutasi_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Pegawai_model->createEmployeeMutasi($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createEmployeeTerminate_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Pegawai_model->createEmployeeTerminate($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function listApprovalEmployeeMatrixById_get($id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getListApprovalEmployeeMatrixById($id, $responseCode);
    $this->response($response, $responseCode);
  }

  public function listHistoryApprovalEmployeeMatrixById_get($id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getListHistoryApprovalEmployeeMatrixById($id, $responseCode);
    $this->response($response, $responseCode);
  }

  public function approvalEmployeeMatrixById_get($id, $id_user_skill_matrix)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getApprovalEmployeeMatrixById($id, $id_user_skill_matrix, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createApprovalEmployeeMatrix_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Pegawai_model->createApprovalEmployeeMatrix($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createLogBook_post()
  {
    $responseCode = '';
    // $rawdata = file_get_contents("php://input");   
    // $data = json_decode($rawdata);

    $data_img = $_FILES["photo"];
    if ($data_img["size"] == 0) {
      $this->response([
        "status" => "error",
        "message" => "Error uploading the file!"
      ], 400);
    }

    $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
    $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
    $data_img['name'] = $data_img['name'];
    $data_img['filename'] = $encrypted_gambar;
    $data_img['urlgambar'] = base_url("/img_logbook/$encrypted_gambar");

    $response = $this->Pegawai_model->createLogBook($data_img, $responseCode);
    $this->response($response, $responseCode);
  }

  public function historyLogBook_get($user_id, $role_id = "")
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getHistoryLogBook($user_id, $role_id, $responseCode);
    $this->response($response, $responseCode);
  }

  public function historyLogBookById_get($id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getHistoryLogBookById($id, $responseCode);
    $this->response($response, $responseCode);
  }

  public function danruLogBookTeam_get($user_id)
  {
    $responseCode = '';
    $response = $this->Pegawai_model->getDanruLogBookTeam($user_id, $responseCode);
    $this->response($response, $responseCode);
  }


  // public function getCareerPegawaiById_get($id){
  //   $response = $this->Pegawai_model->getCareerPegawaiById($id);
  //   $this->response($response);
  // }

  // public function getFileByPkwtID_get($idpkwt){
  //   $response = $this->Document_model->getFileByPkwtID($idpkwt);
  //   $this->response($response);
  // }

  public function downloadMatrix_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);

    // $response = $this->Pegawai_model->getDetailMatrixById($data->user_id, $responseCode);
    // $datas = array(
    //   "dataku" => $response
    // );
    // print_r($data);

    $responseCode = '';
    $this->load->model('Reference_model');
    $skills = $this->Reference_model->skillMatrix($responseCode);

    $role_id = get_info_by_id("sys_users", "role_id", "user_id", $data->user_id);
    $my_matrix = $this->Pegawai_model->getDetailMatrixById($data->user_id, $responseCode);
    if ($role_id == 4) { //danru
      $list_anggota = get_info_as_array("sys_users", "user_id, full_name", "where id_spv = $data->user_id AND role_id = 5 OR (user_id = $data->user_id) ORDER BY role_id ASC");
      $matrix_anggota = array();
      foreach ($list_anggota as $key => $value) {
        $matrix_anggota[$value['full_name']] = $this->Pegawai_model->getDetailMatrixById($value['user_id'], $responseCode);
      }
      $datas = array(
        "full_name" => $data->full_name,
        "user_id" => $data->user_id,
        "list_anggota" => $list_anggota,
        "matrix_anggota" => $matrix_anggota,
        "role_id" => $role_id,
        "my_matrix" => $my_matrix,
        "skills" => $skills
      );
    } else { //satpam atau chief
      $datas = array(
        "full_name" => $data->full_name,
        "user_id" => $data->user_id,
        "role_id" => $role_id,
        "my_matrix" => $my_matrix,
        "skills" => $skills
      );
    }

    $this->load->library('pdf');

    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = "Report matrix " . $data->full_name . ".pdf";
    $this->pdf->load_view('api/matrix', $datas);
    $response = array(
      'status' => 'success',
      'message' => 'Data Found',
      'url' => HOSTNAMEAPI . "/matrix/" . $this->pdf->filename,
      'filename' => $this->pdf->filename
    );
    $this->response($response, 200);
  }

  public function pdf_get()
  {
    $responseCode = '';
    $this->load->model('Reference_model');
    $skills = $this->Reference_model->skillMatrix($responseCode);


    $role_id = get_info_by_id("sys_users", "role_id", "user_id", 4);
    $my_matrix = $this->Pegawai_model->getDetailMatrixById(4, $responseCode);
    if ($role_id == 4) { //danru
      $list_anggota = get_info_as_array("sys_users", "user_id, full_name", "where id_spv = 4 AND role_id = 5 OR (user_id = 4) ORDER BY role_id ASC");
      $matrix_anggota = array();
      foreach ($list_anggota as $key => $value) {
        $matrix_anggota[$value['full_name']] = $this->Pegawai_model->getDetailMatrixById($value['user_id'], $responseCode);
      }
      $datas = array(
        "list_anggota" => $list_anggota,
        "matrix_anggota" => $matrix_anggota,
        "role_id" => $role_id,
        "my_matrix" => $my_matrix,
        "skills" => $skills
      );
    } else { //satpam
      $datas = array(
        "role_id" => $role_id,
        "my_matrix" => $my_matrix,
        "skills" => $skills
      );
    }


    $this->load->library('pdf');

    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = "Report matrix .pdf";
    $this->pdf->load_view('api/matrix', $datas);
  }
}
