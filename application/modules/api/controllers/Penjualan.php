<?php

require APPPATH . 'libraries/REST_Controller.php';

class Penjualan extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Penjualan_model');
    $this->load->helper('common');
  }

  public function listPenjualan_get($user_id)
  {
    $responseCode = '';
    $response = $this->Penjualan_model->getListPenjualan($user_id, $responseCode);
    $this->response($response);
  }

  public function penjualanById_get($id_penjualan)
  {
    $responseCode = '';
    $response = $this->Penjualan_model->getPenjualanById($id_penjualan, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Penjualan_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function updateShowReport_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Penjualan_model->updateShowReport($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createPenjualan_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Penjualan_model->createPenjualan($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function product_get($user_id)
  {
    $responseCode = '';
    $response = $this->Penjualan_model->getProduct($user_id, $responseCode);
    $this->response($response);
  }

  public function downloadReport_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);

    $responseCode = '';
    $penjualan = $this->Penjualan_model->getPenjualanById($data->id_penjualan, $responseCode);
    $datas = array(
      "penjualan" => $penjualan
    );
    // print_r($datas);

    $this->load->library('pdf');

    $this->pdf->setPaper('A4', 'portrait');
    $this->pdf->filename = "Report penjualan " . $data->tanggal . "-" . rand(10, 100) . ".pdf";
    $this->pdf->load_view('api/penjualan', $datas);
    $response = array(
      'status' => 'success',
      'message' => 'Data Found',
      'url' => HOSTNAMEAPI . "/sales_report/" . $this->pdf->filename,
      'filename' => $this->pdf->filename
    );
    $this->response($response, 200);
  }
}
