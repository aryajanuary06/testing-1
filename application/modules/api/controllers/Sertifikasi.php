<?php

require APPPATH . 'libraries/REST_Controller.php';

class Sertifikasi extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Sertifikasi_model');
    $this->load->helper('common');
  }

  public function listSertifikasi_get($user_id)
  {
    $responseCode = '';
    $response = $this->Sertifikasi_model->getListSertifikasi($user_id, $responseCode);
    $this->response($response);
  }

  public function sertifikasiById_get($id_sertifikasi)
  {
    $responseCode = '';
    $response = $this->Sertifikasi_model->getSertifikasiById($id_sertifikasi, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Sertifikasi_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createSertifikasi_post()
  {
    $responseCode = '';

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_sertifikasi/$encrypted_gambar");
    } else {
      $data_img = [];
    }

    $response = $this->Sertifikasi_model->createSertifikasi($data_img, $responseCode);
    $this->response($response, $responseCode);
  }
}
