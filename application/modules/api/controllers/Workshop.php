<?php

require APPPATH . 'libraries/REST_Controller.php';

class Workshop extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Workshop_model');
    $this->load->helper('common');
  }

  public function listWorkshop_get($user_id)
  {
    $responseCode = '';
    $response = $this->Workshop_model->getListWorkshop($user_id, $responseCode);
    $this->response($response);
  }

  public function workshopById_get($id_workshop)
  {
    $responseCode = '';
    $response = $this->Workshop_model->getWorkshopById($id_workshop, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Workshop_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function hapusDetail_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Workshop_model->hapusDetail($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createWorkshop_post()
  {
    $responseCode = '';

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_workshop/$encrypted_gambar");
    } else {
      $data_img = [];
    }
    $arrData = (object) array(
      'user_id' => $this->input->post('user_id'),
      'id_workshop' => $this->input->post('id_workshop'),
    );

    $response = $this->Workshop_model->createWorkshop($arrData, $data_img, $responseCode);
    $this->response($response, $responseCode);
  }


  public function createWorkshopDetail_post()
  {
    $responseCode = '';

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_alat_workshop/$encrypted_gambar");
    } else {
      $data_img = [];
    }
    $arrData = array(      
      'id_workshop_dtl' => $this->input->post('id_workshop_dtl'),
      'nama' => $this->input->post('nama'),
      'kapasitas' => $this->input->post('kapasitas'),
      'id_kat_alat_workshop' => $this->input->post('id_kat_alat_workshop'),
      'img' => $this->input->post('img'),
    );

    $response = $this->Workshop_model->createWorkshopDetail($arrData, $data_img, $this->input->post('id_workshop'), $responseCode);
    $this->response($response, $responseCode);
  }

  // public function createWorkshop_post()
  // {
  //   $responseCode = '';

  //   if ($this->input->post('isUpload') == 'true') {
  //     $data_img = $_FILES["photo"];
  //     if ($data_img["size"] == 0) {
  //       $this->response([
  //         "status" => "error",
  //         "message" => "Error uploading the file!"
  //       ], 400);
  //     }

  //     $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
  //     $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
  //     $data_img['name'] = $data_img['name'];
  //     $data_img['filename'] = $encrypted_gambar;
  //     $data_img['urlgambar'] = base_url("/assets/images/img_penghargaan/$encrypted_gambar");
  //   } else {
  //     $data_img = [];
  //   }

  //   $response = $this->Workshop_model->createWorkshop( $responseCode);
  //   $this->response($response, $responseCode);
  // }

  public function katAlatWorkshop_get()
  {
    $responseCode = '';
    $response = $this->Workshop_model->getKatAlatWorkshop($responseCode);
    $this->response($response);
  }

}
