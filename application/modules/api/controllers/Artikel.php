<?php

require APPPATH . 'libraries/REST_Controller.php';

class Artikel extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('Artikel_model');
  }

  public function getArtikel_get($id_kat_posts){
    $responseCode = '';
    $response = $this->Artikel_model->getArtikel($id_kat_posts, $responseCode);
    $this->response($response, $responseCode);
  }

  public function getLastArtikel_get($id_kat_posts){
    $responseCode = '';
    $response = $this->Artikel_model->getLastArtikel($id_kat_posts, $responseCode);
    $this->response($response, $responseCode);
  }

  public function getArtikelById_get($id){
    $responseCode = '';
    $response = $this->Artikel_model->getArtikelById($id, $responseCode);
    $this->response($response, $responseCode);
  }

}
