<?php

require APPPATH . 'libraries/REST_Controller.php';

class Product extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Product_model');
    $this->load->helper('common');
  }

  public function publicListProduct_get()
  {
    $offset = $this->input->get('offset');
    $search = $this->input->get('search');
    $responseCode = '';
    $response = $this->Product_model->getPublicListProduct($offset, $search, $responseCode);
    $this->response($response);
  }

  public function publicProductById_get($id_product)
  {
    $responseCode = '';
    $response = $this->Product_model->getPublicProductById($id_product, $responseCode);
    $this->response($response);
  }

  public function addToCart_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Product_model->addToCart($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function getCart_get($user_id)
  {
    $responseCode = '';
    $response = $this->Product_model->getCart($user_id, $responseCode);
    $this->response($response);
  }

  public function listProduct_get($user_id)
  {
    $responseCode = '';
    $response = $this->Product_model->getListProduct($user_id, $responseCode);
    $this->response($response);
  }

  public function productById_get($id_product)
  {
    $responseCode = '';
    $response = $this->Product_model->getProductById($id_product, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Product_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createProduct_post()
  {
    $responseCode = '';
    // $this->response($_POST, $responseCode);

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_product/$encrypted_gambar");
    } else {
      $data_img = [];
    }

    $response = $this->Product_model->createProduct($data_img, $responseCode);
    $this->response($response, $responseCode);
  }

  public function katProduct_get()
  {
    $responseCode = '';
    $response = $this->Product_model->getKatProduct($responseCode);
    $this->response($response);
  }

}
