<?php

require APPPATH . 'libraries/REST_Controller.php';

class BM extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('BM_model');
    $this->load->helper('common');
  }

  public function listBB_get($user_id)
  {
    $responseCode = '';
    $response = $this->BM_model->getListBB($user_id, $responseCode);
    $this->response($response);
  }

  public function countPenjual_get($user_id)
  {
    $responseCode = '';
    $response = $this->BM_model->getCountPenjual($user_id, $responseCode);
    $this->response($response);
  }

  public function countPembeli_get($user_id)
  {
    $responseCode = '';
    $response = $this->BM_model->getCountPembeli($user_id, $responseCode);
    $this->response($response);
  }

  public function UmkmByBB_get($BB, $user_id)
  {
    $responseCode = '';
    $response = $this->BM_model->getUmkmByBB($BB, $user_id, $responseCode);
    $this->response($response);
  }

  public function UmkmByBBB_get($BB, $user_id)
  {
    $responseCode = '';
    $response = $this->BM_model->getUmkmByBBB($BB, $user_id, $responseCode);
    $this->response($response);
  }

  public function createPermintaan_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->BM_model->createPermintaan($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function listPermintaanPenjual_get($id_penjual)
  {
    $responseCode = '';
    $response = $this->BM_model->getListPermintaanPenjual($id_penjual, $responseCode);
    $this->response($response);
  }

  public function listPermintaanPembeli_get($id_pembeli)
  {
    $responseCode = '';
    $response = $this->BM_model->getListPermintaanPembeli($id_pembeli, $responseCode);
    $this->response($response);
  }

  public function updateStatusPermintaan_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->BM_model->updateStatusPermintaan($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function umkmById_get($user_id, $bb)
  {
    $responseCode = '';
    $response = $this->BM_model->getUmkmById($user_id, $bb, $responseCode);
    $this->response($response);
  }


}
