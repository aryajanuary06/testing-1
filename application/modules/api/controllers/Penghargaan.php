<?php

require APPPATH . 'libraries/REST_Controller.php';

class Penghargaan extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Penghargaan_model');
    $this->load->helper('common');
  }

  public function listPenghargaan_get($user_id)
  {
    $responseCode = '';
    $response = $this->Penghargaan_model->getListPenghargaan($user_id, $responseCode);
    $this->response($response);
  }

  public function penghargaanById_get($id_penghargaan)
  {
    $responseCode = '';
    $response = $this->Penghargaan_model->getPenghargaanById($id_penghargaan, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Penghargaan_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createPenghargaan_post()
  {
    $responseCode = '';

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_penghargaan/$encrypted_gambar");
    } else {
      $data_img = [];
    }

    $response = $this->Penghargaan_model->createPenghargaan($data_img, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createAspirasi_post()
  {
    $responseCode = '';

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_aspirasi/$encrypted_gambar");
    } else {
      $data_img = [];
    }

    $response = $this->Penghargaan_model->createAspirasi($data_img, $responseCode);
    $this->response($response, $responseCode);
  }

}
