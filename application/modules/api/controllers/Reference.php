<?php

require APPPATH . 'libraries/REST_Controller.php';

class Reference extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('Reference_model');
    $this->load->helper('common');
  }

  public function role_get()
  {
    $responseCode='';
    $response = $this->Reference_model->getRole($responseCode);
    $this->response($response, $responseCode);
  }

  public function alasanMutasi_get()
  {
    $responseCode='';
    $response = $this->Reference_model->alasanMutasi($responseCode);
    $this->response($response, $responseCode);
  }

  public function alasanResign_get()
  {
    $responseCode='';
    $response = $this->Reference_model->alasanResign($responseCode);
    $this->response($response, $responseCode);
  }

  public function client_get()
  {
    $responseCode='';
    $response = $this->Reference_model->client($responseCode);
    $this->response($response, $responseCode);
  }

  public function jabatan_get()
  {
    $responseCode='';
    $response = $this->Reference_model->jabatan($responseCode);
    $this->response($response, $responseCode);
  }

  public function skillMatrix_get()
  {
    $responseCode='';
    $response = $this->Reference_model->skillMatrix($responseCode);
    $this->response($response, $responseCode);
  }


}
