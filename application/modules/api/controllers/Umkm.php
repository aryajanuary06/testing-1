<?php

require APPPATH . 'libraries/REST_Controller.php';

class Umkm extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Umkm_model');
    $this->load->helper('common');

    header('Access-Control-Allow-Origin: http://localhost:3001');
    header("Content-Type: application/json; charset=UTF-8");
    Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
  }

  public function listUmkm_get()
  {
    $offset = $this->input->get('offset');
    $search = $this->input->get('search');
    $responseCode = '';
    $response = $this->Umkm_model->getListUmkm($offset, $search, $responseCode);
    $this->response($response);
  }

  public function umkmById_get($user_id)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getUmkmById($user_id, $responseCode);
    $this->response($response);
  }

  public function editUmkmById_get($user_id)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getEditUmkmById($user_id, $responseCode);
    $this->response($response);
  }

  public function countUmkmById_get($user_id)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getCountUmkmById($user_id, $responseCode);
    $this->response($response);
  }

  public function produksiById_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Umkm_model->getProduksiById($data, $responseCode);
    $this->response($response);
  }

  public function penjualanById_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Umkm_model->getPenjualanById($data, $responseCode);
    $this->response($response);
  }

  public function bbById_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Umkm_model->getBbById($data, $responseCode);
    $this->response($response);
  }

  public function search_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Umkm_model->search($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createUmkm_post()
  {
    $responseCode = '';

    if ($this->input->post('isUpload') == 'true') {
      $data_img = $_FILES["photo"];
      if ($data_img["size"] == 0) {
        $this->response([
          "status" => "error",
          "message" => "Error uploading the file!"
        ], 400);
      }

      $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
      $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
      $data_img['name'] = $data_img['name'];
      $data_img['filename'] = $encrypted_gambar;
      $data_img['urlgambar'] = base_url("/assets/images/img_umkm/$encrypted_gambar");
    } else {
      $data_img = [];
    }

    $response = $this->Umkm_model->createUmkm($data_img, $responseCode);
    $this->response($response, $responseCode);
  }

  public function topten_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Umkm_model->getTopten($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function katUMKM_get()
  {
    $responseCode = '';
    $response = $this->Umkm_model->getKatUMKM($responseCode);
    $this->response($response);
  }

  public function listProvinsi_get()
  {
    $responseCode = '';
    $response = $this->Umkm_model->getListProvinsi($responseCode);
    $this->response($response);
  }

  public function listKabByProvId_get($prov_id)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getListKabByProvId($prov_id, $responseCode);
    $this->response($response);
  }

  public function listKecByKabId_get($kab_id)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getListKecByKabId($kab_id, $responseCode);
    $this->response($response);
  }

  public function listKelByKecId_get($kec_id)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getListKelByKecId($kec_id, $responseCode);
    $this->response($response);
  }

  public function workshopById_get($id_workshop)
  {
    $responseCode = '';
    $response = $this->Umkm_model->getWorkshopById($id_workshop, $responseCode);
    $this->response($response);
  }
}
