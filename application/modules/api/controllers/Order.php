<?php

require APPPATH . 'libraries/REST_Controller.php';

class Order extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Order_model');
    $this->load->helper('common');
  }

  public function publicListProduct_get()
  {
    $responseCode = '';
    $response = $this->Order_model->getPublicListProduct($responseCode);
    $this->response($response);
  }

  public function publicProductById_get($id_product)
  {
    $responseCode = '';
    $response = $this->Order_model->getPublicProductById($id_product, $responseCode);
    $this->response($response);
  }

  public function addToCart_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Order_model->addToCart($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function getCart_get($user_id)
  {
    $responseCode = '';
    $response = $this->Order_model->getCart($user_id, $responseCode);
    $this->response($response);
  }

  public function getCartCheckout_get($user_id)
  {
    $responseCode = '';
    $response = $this->Order_model->getCartCheckout($user_id, $responseCode);
    $this->response($response);
  }

  public function updateChecked_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Order_model->updateChecked($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function updateQty_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Order_model->updateQty($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function deleteCart_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Order_model->deleteCart($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function checkout_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Order_model->checkout($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function test_email_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Order_model->test_email($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function testEmail_get()
  {
    $responseCode = '';
    $response = $this->Order_model->test_email([], $responseCode);
    $this->response($response, $responseCode);
  }


}
