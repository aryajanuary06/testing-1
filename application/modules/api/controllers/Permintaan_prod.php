<?php

require APPPATH . 'libraries/REST_Controller.php';

class Permintaan_prod extends REST_Controller
{

  // construct
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Permintaan_prod_model');
    $this->load->helper('common');
  }

  public function listPermintaan_prod_get($user_id)
  {
    $responseCode = '';
    $response = $this->Permintaan_prod_model->getListPermintaan_prod($user_id, $responseCode);
    $this->response($response);
  }

  public function permintaan_prodById_get($id_permintaan_prod)
  {
    $responseCode = '';
    $response = $this->Permintaan_prod_model->getPermintaan_prodById($id_permintaan_prod, $responseCode);
    $this->response($response);
  }

  public function hapus_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Permintaan_prod_model->hapus($data, $responseCode);
    $this->response($response, $responseCode);
  }

  public function createPermintaan_prod_post()
  {
    $responseCode = '';
    $rawdata = file_get_contents("php://input");
    $data = json_decode($rawdata);
    $response = $this->Permintaan_prod_model->createPermintaan_prod($data, $responseCode);
    $this->response($response, $responseCode);
  }
}
