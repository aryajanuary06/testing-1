<?php

class Reference_model extends CI_Model
{
  public function getRole(&$responseCode)
  {

    $lstRole = get_info_as_array("sys_roles", "roles_id, roles_name as name, roles_desc as 'desc'", "WHERE roles_name != 'Super Admin Web'");
    if ($lstRole) {
      $response = [
        'status' => 'success',
        'message' => 'Data Available on Database',
        'data' => $lstRole
      ];
      $responseCode = 200;
    } else {
      $response = [
        'status' => 'error',
        'message' => 'Data Not Available on Database'
      ];
      $responseCode = 404;
    }

    return $response;
  }

  public function alasanMutasi(&$responseCode)
  {

    $lstWilayah = get_info_as_array("ref_alasan_mutasi", "id_alasan_mutasi, alasan_mutasi", "order by id_alasan_mutasi asc");
    if ($lstWilayah) {
      $response = [
        'status' => 'success',
        'message' => 'Data Available on Database',
        'data' => $lstWilayah
      ];
      $responseCode = 200;
    } else {
      $response = [
        'status' => 'error',
        'message' => 'Data Not Available on Database'
      ];
      $responseCode = 404;
    }

    return $response;
  }

  public function alasanResign(&$responseCode)
  {

    $lstWilayah = get_info_as_array("ref_alasan_resign", "id_alasan_resign, alasan_resign", "order by id_alasan_resign asc");
    if ($lstWilayah) {
      $response = [
        'status' => 'success',
        'message' => 'Data Available on Database',
        'data' => $lstWilayah
      ];
      $responseCode = 200;
    } else {
      $response = [
        'status' => 'error',
        'message' => 'Data Not Available on Database'
      ];
      $responseCode = 404;
    }

    return $response;
  }

  public function client(&$responseCode)
  {

    $lstWilayah = get_info_as_array("ref_client", "id_client, nama_client", "");
    if ($lstWilayah) {
      $response = [
        'status' => 'success',
        'message' => 'Data Available on Database',
        'data' => $lstWilayah
      ];
      $responseCode = 200;
    } else {
      $response = [
        'status' => 'error',
        'message' => 'Data Not Available on Database'
      ];
      $responseCode = 404;
    }

    return $response;
  }

  public function jabatan(&$responseCode)
  {

    $lstWilayah = get_info_as_array("ref_jabatan", "id_jabatan, nama_jabatan", "");
    if ($lstWilayah) {
      $response = [
        'status' => 'success',
        'message' => 'Data Available on Database',
        'data' => $lstWilayah
      ];
      $responseCode = 200;
    } else {
      $response = [
        'status' => 'error',
        'message' => 'Data Not Available on Database'
      ];
      $responseCode = 404;
    }

    return $response;
  }

  public function skillMatrix(&$responseCode)
  {

    $lstWilayah = get_info_as_array("ref_skill_matrix", "id_skill_matrix, nama_skill_matrix", "");
    if ($lstWilayah) {
      $response = [
        'status' => 'success',
        'message' => 'Data Available on Database',
        'data' => $lstWilayah
      ];
      $responseCode = 200;
    } else {
      $response = [
        'status' => 'error',
        'message' => 'Data Not Available on Database'
      ];
      $responseCode = 404;
    }

    return $response;
  }
  

}


// todo lanjut api utk input2