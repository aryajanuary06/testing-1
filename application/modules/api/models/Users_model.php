<?php

// extends class Model
require APPPATH . '/libraries/Cryptlib.php';
class Users_model extends CI_Model
{

  public function registerUser($data, &$responseCode)
  {
    $converter = new Encryption;
    $arrData = [
      'email' => $data->email,
      'username' => $data->username,
      'pwd' =>  $converter->encode($data->password),
      // 'full_name' => $data->full_name,
      // 'no_telp' => $data->no_telp,
      // 'role_id' => $data->role,
      'role_id' => 2,
      'create_at' => getsysdate()
    ];
    $q = $this->db->where('email', $data->email)->get('sys_users');
    if ($q->num_rows() > 0) {
      $response = [
        "status" => "conflict",
        "message" => "Email already exist.",
      ];
      $responseCode = 409;
    } else {
      if ($this->db->insert("sys_users", $arrData)) {
        // $user_id = $this->db->insert_id();

        $response = [
          "status" => "success",
          "message" => "Daftar berhasil, silakan login dengan akun Anda",
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => MSG_INSERT_FAILED,
        ];
        $responseCode = 404;
      }
    }
    return $response;
  }

  public function resetPassword($data, &$responseCode)
  {
    $this->load->helper('email');
    $converter = new Encryption;
    $pass = rand();
    $arrData = [
      'pwd' => $converter->encode($pass)
    ];

    $q = $this->db->where('email', $data->email)->get('sys_users');
    if ($q->num_rows() > 0) {
      $dtUser = $q->row();
      $this->db->where('email', $data->email);

      if ($this->db->update("sys_users", $arrData)) {
        sendEmail("$data->email", $pass);

        $response = [
          "status" => "success",
          "message" => MSG_INSERT_SUCCESS
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => MSG_INSERT_FAILED,
        ];
        $responseCode = 404;
      }
    } else {
      $response = [
        "status" => "conflict",
        "message" => "Email " . $data->email . " not exist.",
      ];
      $responseCode = 409;
    }
    return $response;
  }

  public function getUserById($user_id, &$responseCode)
  {
    $this->db->select("user_id, email, username, full_name, no_telp, role_id, concat('" . HOSTNAMEAPI . "/assets/images/img_profile/',img_profile) as img_profile, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',img_umkm_pp) as img_umkm_pp");
    $this->db->from("sys_users");
    $this->db->where("user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function userLogin($data, &$responseCode)
  {
    // print_r($data);
    $converter = new Encryption;
    $pwd = $converter->encode($data->password);
    $email = $data->email;

    $this->db->select("user_id, email, username, pwd, full_name, no_telp, role_id, img_profile, is_show_sales_report");
    $this->db->from("sys_users");
    $this->db->where("(email='$email' OR username='$email') AND pwd = '$pwd'  AND is_suspend = 'Tidak'");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $dtUser = $query->row();

      // $this->db->select("concat(j.nama_client, ' - ', j.alamat) as nama_client");
      // $this->db->from("tbl_penugasan AS p");
      // $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
      // $this->db->where("p.user_id = '$dtUser->user_id' ");
      // $query = $this->db->get();
      // $rows = $query->row();
      // $unit = get_info_by_id("ref_posbindu", "nama_posbindu", "id_posbindu", $dtUser->id_unit);
      // $this->db->select("mr.id_menu, am.menu_label, am.icon_menu");
      // $this->db->from("sys_menu_role AS mr");
      // $this->db->join("sys_admin_menu AS am", "mr.id_menu = am.id_menu", "INNER");
      // $this->db->where("mr.roles_id='$dtUser->role_id' AND am.parent_id =0 AND am.is_active ='Y' AND am.is_mobile_app =1");
      // $this->db->order_by('am.sort_order ASC');
      // $query2 = $this->db->get();
      // $menu = $query2->result_array();

      $roles = get_info_by_id("sys_roles", "roles_name", "roles_id", $dtUser->role_id);
      $name_umkm = get_info_by_id("tbl_umkm", "title", "user_id", $dtUser->user_id);
      $img_profile = "";
      if ($dtUser->img_profile) {
        $img_profile = HOSTNAMEAPI . "/assets/images/img_profile/" . $dtUser->img_profile;
      }

      $response = [
        "status" => "success",
        "message" => "Login successfull",
        'user_id' => $dtUser->user_id,
        'username' => $dtUser->username,
        'no_telp' => $dtUser->no_telp,
        'img_profile' => $img_profile,
        'email' => $dtUser->email,
        'full_name' => $dtUser->full_name,
        'role' => $dtUser->role_id,
        'role_name' => $roles,
        'umkm' => $name_umkm,
        'is_show_sales_report' => $dtUser->is_show_sales_report,
        // 'penugasan' => isset($rows->nama_client) ? $rows->nama_client : "",
        // 'menu' => $menu
      ];
      $responseCode = 202;
    } else {
      $response = [
        "status" => "error",
        "message" => "Login failed. Incorrect username or password"
      ];
      $responseCode = 404;
    }
    return $response;
  }


  public function editProfile($data, $data2, &$responseCode)
  {
    $minPassword = 3;
    $arrData = [
      'full_name' => $this->input->post('full_name'),
      'email' => $this->input->post('email'),
      // 'username' => $this->input->post('username'),
      'no_telp' => $this->input->post('no_telp'),
      'img_profile' => $data['filename'],
    ];

    if ($this->input->post('password') != "") {
      $converter = new Encryption;
      $converted = $converter->encode($this->input->post('password'));
      $arrData += ['pwd' =>  $converted];

      if (strlen($this->input->post('password')) < $minPassword) {
        $response = [
          "status" => "error",
          "message" => "Password must at least $minPassword characters",
        ];
        $responseCode = 409;
        return $response;
      }
    }
    $p = $this->db
      ->where('username', $this->input->post('username'))
      ->where('email', $this->input->post('email'))
      ->where('user_id !=', $this->input->post('user_id'))
      ->get('sys_users');
    if ($p->num_rows() < 1) {
      $q = $this->db->where('user_id', $this->input->post('user_id'))->get('sys_users');

      if ($q->num_rows() > 0) {

        //upload image background untuk profile dan UMKM
        $url_bg = "";
        if ($data2["size"] > 0 && $this->input->post('isUpload2') == 'true') {
          if (move_uploaded_file($data2['tmp_name'], './assets/images/img_umkm_pp/' . $data2['filename'])) {
            $source_path = FCPATH . 'assets/images/img_umkm_pp/';
            $target_path = FCPATH . 'assets/images/img_umkm_pp/';

            if ($data2["size"] > 36627) {
              resizeImage($data2['filename'], $source_path, $target_path);
            }

            $arrD = [
              'img_umkm_pp' => $data2['filename'],
            ];
            $this->db->where('user_id', $this->input->post('user_id'))->update('sys_users', $arrD);
            if ($q->row()->img_umkm_pp != null) {
              unlink('./assets/images/img_umkm_pp/' . $q->row()->img_umkm_pp);
            }
            $url_bg = $data2['urlgambar'];
          }
        }

        //upload image profile user
        if ($data["size"] > 0 && $this->input->post('isUpload') == 'true') {
          if (move_uploaded_file($data['tmp_name'], './assets/images/img_profile/' . $data['filename'])) {
            $source_path = FCPATH . 'assets/images/img_profile/';
            $target_path = FCPATH . 'assets/images/img_profile/';

            if ($data["size"] > 36627) {
              resizeImage($data['filename'], $source_path, $target_path);
            }

            $this->db->where('user_id', $this->input->post('user_id'))->update('sys_users', $arrData);
            if ($q->row()->img_profile != null) {
              unlink('./assets/images/img_profile/' . $q->row()->img_profile);
            }

            $response = [
              "abc" => resizeImage($data['filename'], $source_path, $target_path),
              "status" => "success",
              "message" => "Data Profile berhasil diupdate",
              "url" => $data['urlgambar'],
              "url_bg" => $url_bg,
              "query" => $this->db->last_query(),

            ];
            $responseCode = 201;
          } else {
            $response = array(
              "status" => "error",
              "error" => true,
              "message" => "Error uploading the file!"
            );
            $responseCode = 409;
          }
        } else {
          $arrData = [
            'full_name' => $this->input->post('full_name'),
            'email' => $this->input->post('email'),
            // 'username' => $this->input->post('username'),
            'no_telp' => $this->input->post('no_telp'),
          ];
          if ($this->input->post('password') != "") {
            $converter = new Encryption;
            $converted = $converter->encode($this->input->post('password'));
            $arrData += ['pwd' =>  $converted];

            if (strlen($this->input->post('password')) < $minPassword) {
              $response = [
                "status" => "error",
                "message" => "Password must at least $minPassword characters",
              ];
              $responseCode = 409;
              return $response;
            }
          }
          $this->db->where('user_id', $this->input->post('user_id'))->update('sys_users', $arrData);

          $response = [
            "status" => "success",
            "message" => "Data Profile berhasil diupdate",
            "url" => $_POST,
            "url_bg" => $url_bg,
            "query" => $this->db->last_query(),
          ];
          $responseCode = 201;
        }
      } else {
        $response = [
          "status" => "error",
          "message" => "Invalid Id User",
        ];
        $responseCode = 409;
      }
    } else {
      $response = [
        "status" => "conflict",
        "message" => "Email/Username already exist.",
      ];
      $responseCode = 409;
    }
    return $response;
  }

  // public function updatePP($user_id, $data, &$responseCode)
  // {
  //   // $q = $this->db->where('user_id', $user_id)->get('sys_users');
  //   // if ($q->num_rows() > 0) {
  //   if (move_uploaded_file($data['tmp_name'], './photos/' . $data['filename'])) {
  //     if ($q->row()->img_profile == null) {
  //       $this->db->where('user_id', $user_id)->update('sys_users', ['img_profile' => $data['filename']]);
  //       $response = array(
  //         "status" => "error",
  //         "error" => true,
  //         "message" => "File uploaded successfully",
  //         "url" => $data['urlgambar']
  //       );
  //       $responseCode = 201;
  //     } else {
  //       unlink('./photos/' . $q->row()->img_profile);
  //       $this->db->where('user_id', $user_id)->update('sys_users', ['img_profile' => $data['filename']]);
  //       $response = array(
  //         "status" => "success",
  //         "error" => false,
  //         "message" => "File updated successfully, Old file removed",
  //         "url" => $data['urlgambar']
  //       );
  //       $responseCode = 201;
  //     }
  //   } else {
  //     $response = array(
  //       "status" => "error",
  //       "error" => true,
  //       "message" => "Error uploading the file!"
  //     );
  //     $responseCode = 202;
  //   }
  //   // } else {
  //   //   $response = array(
  //   //     "status" => "error",
  //   //     "error" => true,
  //   //     "message" => "User Not found!"
  //   //   );
  //   //   $responseCode = 202;
  //   // }
  //   return $response;
  // }
}
