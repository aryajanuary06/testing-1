<?php

// extends class Model
class BB_model extends CI_Model
{

  public function getListBB($user_id, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_bahan_baku");
    $this->db->where("user_id= " . $user_id);
    $this->db->order_by("id_bahan_baku DESC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getBBById($id_bahan_baku, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_bahan_baku");
    $this->db->where("id_bahan_baku= " . $id_bahan_baku);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }


  public function createBB($data, &$responseCode)
  {
    $arrData = [
      'bulan' => $data->bulan,
      'name' => $data->name,
      'user_id' => $data->user_id,
      'desc' => $data->desc,
      'jml' => $data->jml,
      'id_kat_bahan_baku' => $data->id_kat_bahan_baku,
      'id_satuan_bb' => $data->id_satuan_bb,
      'create_at' => getsysdate(),
    ];
    // return $data;
    if ($data->id_bahan_baku == "") {
      if ($this->db->insert("tbl_bahan_baku", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_bahan_baku', $data->id_bahan_baku);
      if ($this->db->update("tbl_bahan_baku", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal diupdate',
        ];
        $responseCode = 404;
      }
    }
    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_bahan_baku", $data->id);
    if ($this->db->delete("tbl_bahan_baku")) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getKatBB(&$responseCode)
  {
    $this->db->select("id_kat_bahan_baku, name");
    $this->db->from("tbl_kat_bahan_baku");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getSatuanBB(&$responseCode)
  {
    $this->db->select("id_setting as id_satuan_bb, value_setting as name");
    $this->db->from("sys_settings");
    $this->db->where("category_setting = 'satuan_bb'");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

}
