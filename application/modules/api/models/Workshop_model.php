<?php

// extends class Model
class Workshop_model extends CI_Model
{

  public function getListWorkshop($user_id, &$responseCode)
  {
    $this->db->select("id_workshop, lokasi");
    $this->db->from("tbl_workshop");
    $this->db->where("user_id= '" . $user_id . "' AND is_temp = 0");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getWorkshopById($id_workshop, &$responseCode)
  {
    $this->db->select("id_workshop, lokasi, sdm, concat('" . HOSTNAMEAPI . "/assets/images/img_workshop/',img) as img_workshop");
    $this->db->from("tbl_workshop");
    $this->db->where("id_workshop= " . $id_workshop);
    $this->db->order_by('id_workshop DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("id_workshop_dtl, nama, kapasitas, id_kat_alat_workshop, img, concat('" . HOSTNAMEAPI . "/assets/images/img_alat_workshop/',img) as img_alat_workshop");
      $this->db->from("tbl_workshop_dtl");
      $this->db->where("id_workshop= '" . $id_workshop . "' ");
      $this->db->order_by('nama DESC');
      $query2 = $this->db->get();
      $workshop_dtl = $query2->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        'workshop_dtl' => $workshop_dtl
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createWorkshopDetail($data, $data_img, $id_workshop, &$responseCode)
  {
    if ($id_workshop == "") {
      $arrTblWs = [
        'user_id' => $this->input->post('user_id'),
        'lokasi' => $this->input->post('lokasi'),
        'sdm' => $this->input->post('sdm'),
        'is_temp' => 1,
        'create_at' => getsysdate(),
      ];
      $this->db->insert("tbl_workshop", $arrTblWs);
      $id_workshop = $this->db->insert_id();
    }
    if (count($data_img) > 0) {
      if ($data['img'] != "") {
        unlink('./assets/images/img_alat_workshop/' . $data['img']);
      }
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_alat_workshop/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_alat_workshop/';
        $target_path = FCPATH . 'assets/images/img_alat_workshop/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
        $arrData = [
          'id_workshop' => $id_workshop,
          'nama' => $data['nama'],
          'kapasitas' => $data['kapasitas'],
          'id_kat_alat_workshop' => $data['id_kat_alat_workshop'],
          'img' => $data_img['filename'],
          // 'is_temp' => 1,
        ];
      }
    } else {
      $arrData = [
        'id_workshop' => $id_workshop,
        'nama' => $data['nama'],
        'kapasitas' => $data['kapasitas'],
        'id_kat_alat_workshop' => $data['id_kat_alat_workshop'],
        // 'is_temp' => 1,
      ];
    }

    if ($this->input->post('id_workshop_dtl') == "") {
      $arrData['create_at'] = getsysdate();
      $this->db->insert("tbl_workshop_dtl", $arrData);
    } else {
      $arrData['update_at'] = getsysdate();
      $this->db->where('id_workshop_dtl', $data['id_workshop_dtl']);
      $this->db->update("tbl_workshop_dtl", $arrData);
    }

    $this->db->select("id_workshop_dtl, nama, kapasitas, id_kat_alat_workshop, concat('" . HOSTNAMEAPI . "/assets/images/img_alat_workshop/',img) as img_alat_workshop");
    $this->db->from("tbl_workshop_dtl");
    $this->db->where("id_workshop= '" . $id_workshop . "' ");
    $this->db->order_by('nama ASC');
    $query2 = $this->db->get();
    $workshop_dtl = $query2->result_array();

    $response = [
      "status" => "success",
      "message" => 'Data berhasil',
      "data" => $workshop_dtl,
      "old_img" => $data['img'],
      "id_workshop" => $id_workshop
      
    ];
    $responseCode = 201;

    return $response;
  }

  public function createWorkshop($data, $data_img, &$responseCode)
  {
    if (count($data_img) > 0) {
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_workshop/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_workshop/';
        $target_path = FCPATH . 'assets/images/img_workshop/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
        $arrData = [
          'user_id' => $data->user_id,
          'lokasi' => $this->input->post('lokasi'),
          'sdm' => $this->input->post('sdm'),
          'img' => $data_img['filename'],
          'is_temp' => 0,
        ];
      }
    } else {
      $arrData = [
        'user_id' => $data->user_id,
        'lokasi' => $this->input->post('lokasi'),
        'sdm' => $this->input->post('sdm'),
        'is_temp' => 0,
      ];
    }

    if ($data->id_workshop == "") { //baru & blm pernah add detail
      $arrData['create_at'] = getsysdate();
      $this->db->insert("tbl_workshop", $arrData);
    } else { // (baru & pernah add detail || lama)

      $arrData['update_at'] = getsysdate();
      $this->db->where('id_workshop', $data->id_workshop);
      $this->db->update("tbl_workshop", $arrData);


      // $arrTblWsDtl = [
      //   'is_temp' => 0,
      //   'update_at' => getsysdate(),
      // ];

      // $this->db->where('user_id', $data->user_id);
      // $this->db->update("tbl_workshop_dtl", $arrTblWsDtl);
    }

    $response = [
      "status" => "success",
      "message" => 'Data berhasil',
    ];
    $responseCode = 201;

    return $response;
  }

  public function createWorkshop222222(&$responseCode)
  {
    $arrData = [
      'user_id' => $this->input->post('user_id'),
      'lokasi' => $this->input->post('lokasi'),
      'sdm' => $this->input->post('sdm'),
      'create_at' => getsysdate(),
    ];
    $data_dtl = $this->input->post('detail');
    if ($this->input->post('id_workshop') == "") {
      if ($this->db->insert("tbl_workshop", $arrData)) {
        $insert_id = $this->db->insert_id();
        for ($i = 0; $i < count($data_dtl); $i++) {

          if ($data_dtl['isUpload'] == 'true') {
            $data_img = $_FILES["photo"];
            if ($data_img["size"] == 0) {
              $data_img = [];
            } else {

              $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
              $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
              // $data_img['name'] = $data_img['name'];
              $data_img['filename'] = $encrypted_gambar;
            }
          } else {
            $data_img = [];
          }

          // $this->createWorkshopDetail($data_dtl[$i], $data_img, $insert_id);
        }

        $response = [
          "status" => "success",
          "message" => 'Data berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_workshop', $this->input->post('id_workshop'));
      if ($this->db->update("tbl_workshop", $arrData)) {

        for ($i = 0; $i < count($data_dtl); $i++) {

          if ($data_dtl['isUpload'] == 'true') {
            $data_img = $_FILES["photo"];
            if ($data_img["size"] == 0) {
              $data_img = [];
            } else {

              $ext = pathinfo($data_img['name'], PATHINFO_EXTENSION);
              $encrypted_gambar = $data_img['name'] . date("Y-m-d H-i-s") . "." . $ext;
              // $data_img['name'] = $data_img['name'];
              $data_img['filename'] = $encrypted_gambar;
            }
          } else {
            $data_img = [];
          }

          // $this->createWorkshopDetail($data_dtl[$i], $data_img, $this->input->post('id_workshop'));
        }

        $response = [
          "status" => "success",
          "message" => 'Data berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal diupdate',
        ];
        $responseCode = 404;
      }
    }
    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_workshop", $data->id);
    if ($this->db->delete("tbl_workshop")) {
      $this->db->where("id_workshop", $data->id);
      $this->db->delete("tbl_workshop_dtl");
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function hapusDetail($data, &$responseCode)
  {
    $this->db->where("id_workshop_dtl", $data->id);
    if ($this->db->delete("tbl_workshop_dtl")) {
      if ($data->img != "") {
        unlink('./assets/images/img_alat_workshop/' . $data->img);
      }

      $this->db->select("id_workshop_dtl, nama, kapasitas, id_kat_alat_workshop, concat('" . HOSTNAMEAPI . "/assets/images/img_alat_workshop/',img) as img_alat_workshop");
      $this->db->from("tbl_workshop_dtl");
      $this->db->where("id_workshop= '" . $data->id_workshop . "' ");
      $this->db->order_by('nama ASC');
      $query2 = $this->db->get();
      $workshop_dtl = $query2->result_array();

      $response = [
        "status" => "success",
        "data" => $workshop_dtl,
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getKatAlatWorkshop(&$responseCode)
  {
    $this->db->select("id_kat_alat_workshop, name");
    $this->db->from("tbl_kat_alat_workshop");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
}
