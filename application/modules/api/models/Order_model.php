<?php

// extends class Model
class Order_model extends CI_Model
{

  public function getPublicListProduct(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_product/',img) as img_product");
    $this->db->from("tbl_product");
    $this->db->order_by("id_product DESC");
    // $this->db->where("user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPublicProductById($id_product, &$responseCode)
  {
    $this->db->select("id_product, tp.name, tp.price, tp.id_kat_prod, tp.desc, tkp.name AS kat_name, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product");
    $this->db->from("tbl_product tp");
    $this->db->join("tbl_kat_prod AS tkp", "tp.id_kat_prod = tkp.id_kat_prod", "LEFT");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "LEFT");
    $this->db->where("id_product= " . $id_product);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function addToCart($data, &$responseCode)
  {
    $this->db->select("qty");
    $this->db->from("tbl_cart");
    $this->db->where("user_id='$data->user_id' AND id_product='$data->id_product' AND is_buy = 0");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      //update qty cart
      $dtCart = $query->row();
      $arrData = [
        'user_id' => $data->user_id,
        'id_product' => $data->id_product,
        'qty' => $data->qty + $dtCart->qty,
        'create_at' => getsysdate(),
      ];
      $this->db->where("user_id='$data->user_id' AND id_product='$data->id_product' AND is_buy = 0");
      if ($this->db->update("tbl_cart", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Keranjang berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'gagal diupdate',
        ];
        $responseCode = 404;
      }
    } else {
      //insert to cart
      $arrData = [
        'user_id' => $data->user_id,
        'id_product' => $data->id_product,
        'qty' => $data->qty,
        'is_ceklis' => 1,
        'create_at' => getsysdate(),
      ];
      if ($this->db->insert("tbl_cart", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Keranjang berhasil ditambahkan',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'gagal dibuat',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function getCart($user_id, &$responseCode)
  {
    $this->db->select("tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
    $this->db->from("tbl_cart tc");
    $this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
    $this->db->where("tc.user_id= ' $user_id ' AND is_buy = 0");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getCartCheckout($user_id, &$responseCode)
  {
    $this->db->select("tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
    $this->db->from("tbl_cart tc");
    $this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
    $this->db->where("tc.user_id= ' $user_id ' AND is_buy = 0 AND tc.is_ceklis = 1");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function updateChecked($data, &$responseCode)
  {
    $arrData = [
      'is_ceklis' => $data->is_ceklis,
      'update_at' => getsysdate(),
    ];

    $this->db->where_in('id_cart', $data->id_cart);
    if ($this->db->update("tbl_cart", $arrData)) {

      $this->db->select("tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("tbl_cart tc");
      $this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
      $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
      $this->db->where("tc.user_id= " . $data->user_id . " AND is_buy = 0");

      $query = $this->db->get();
      $rows = $query->result_array();

      $response = [
        "status" => "success",
        "message" => 'Berhasil diupdate',
        'data' => $rows
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Gagal diupdate',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function updateQty($data, &$responseCode)
  {
    $arrData = [
      'qty' => $data->qty,
      'update_at' => getsysdate(),
    ];

    $this->db->where('id_cart', $data->id_cart);
    if ($this->db->update("tbl_cart", $arrData)) {
      $this->db->select("tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("tbl_cart tc");
      $this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
      $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
      $this->db->where("tc.user_id= " . $data->user_id . " AND is_buy = 0");

      $query = $this->db->get();
      $rows = $query->result_array();

      $response = [
        "status" => "success",
        'data' => $rows,
        "message" => 'Quantity berhasil diupdate',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Quantity gagal diupdate',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function deleteCart($data, &$responseCode)
  {
    $this->db->where("id_cart", $data->id_cart);
    if ($this->db->delete("tbl_cart")) {
      $this->db->select("tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("tbl_cart tc");
      $this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
      $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
      $this->db->where("tc.user_id= " . $data->user_id . " AND is_buy = 0");

      $query = $this->db->get();
      $rows = $query->result_array();

      $response = [
        "status" => "success",
        'data' => $rows,
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function test_email($data, &$responseCode)
  {
    $this->load->helper('email');
    $arrDataOrder = [
      'penerima' => 'penerimapenerimapenerimapenerimapenerima',
      'telp' => '06895486984956',
      'alamat' => 'alamatalamatalamatalamatalamat',
      'email' => 'email',
      'create_at' => getsysdate(),
    ];
    $a = sendEmailInvoice("yanataryana545@gmail.com", "5", $arrDataOrder);

    if ($a) {

      $response = [
        "status" => "success",
        "message" => $a,
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => $a,
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function checkout($data, &$responseCode)
  {
    $this->load->helper('email');
    $arrDataOrder = [
      'penerima' => $data->name,
      'telp' => $data->telp,
      'alamat' => $data->alamat,
      'email' => $data->email,
      'create_at' => getsysdate(),
    ];

    if ($this->db->insert("tbl_order", $arrDataOrder)) {
      $id_order = $this->db->insert_id();
      $arrData = [
        'is_buy' => 1,
        'id_order' => $id_order,
        'update_at' => getsysdate(),
      ];

      $this->db->where("user_id = '" . $data->user_id . "' AND is_ceklis = 1 AND is_buy = 0 ");
      if ($this->db->update("tbl_cart", $arrData)) {
        sendEmailInvoice("$data->email", $id_order, $arrDataOrder);

        $response = [
          "status" => "success",
          "message" => 'Order berhasil',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Order gagal',
        ];
        $responseCode = 404;
      }
    } else {
      $response = [
        "status" => "error",
        "message" => 'Order gagal',
      ];
      $responseCode = 404;
    }

    return $response;
  }
}
