<?php

// extends class Model
class Sertifikasi_model extends CI_Model
{

  public function getListSertifikasi($user_id, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_sertifikasi/',img) as img_sertifikasi");
    $this->db->from("tbl_sertifikasi");
    $this->db->where("user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getSertifikasiById($id_sertifikasi, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_sertifikasi/',img) as img_sertifikasi");
    $this->db->from("tbl_sertifikasi");
    $this->db->where("id_sertifikasi= " . $id_sertifikasi);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function search($data, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_sertifikasi/',img) as img_sertifikasi");
    $this->db->from("tbl_sertifikasi");
    $this->db->where("user_id= " . $data->user_id);
    if (!empty($data->keywords)) {
      $this->db->or_like("title", $data->keywords);
    }
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createSertifikasi($data_img, &$responseCode)
  {
    if (count($data_img) > 0) {
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_sertifikasi/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_sertifikasi/';
        $target_path = FCPATH . 'assets/images/img_sertifikasi/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
      }
      $arrData = [
        'user_id' => $this->input->post('user_id'),
        'title' => $this->input->post('title'),
        'desc' => $this->input->post('desc'),
        'img' => $data_img['filename'],
        'create_at' => getsysdate(),
      ];
    } else {
      $arrData = [
        'user_id' => $this->input->post('user_id'),
        'title' => $this->input->post('title'),
        'desc' => $this->input->post('desc'),
        'create_at' => getsysdate(),
      ];
    }
    // $tgl = json_decode($this->input->post('tgl'));
    if ($this->input->post('id_sertifikasi') == "") {
      if ($this->db->insert("tbl_sertifikasi", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_sertifikasi', $this->input->post('id_sertifikasi'));
      if ($this->db->update("tbl_sertifikasi", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal diupdate',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_sertifikasi", $data->id);
    if ($this->db->delete("tbl_sertifikasi")) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }
}
