<?php

// extends class Model
class Pegawai_model extends CI_Model
{

  public function getDanruByClient($user_id, &$responseCode)
  {
    $id_client = get_info_by_id('ref_client', 'id_client', 'user_id', $user_id);
    $this->db->select("su.user_id, su.email, j.nama_client, j.alamat, su.full_name, tk.tgl_awal_kontrak, tk.tgl_akhir_kontrak, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("tbl_penugasan AS p");
    $this->db->join("sys_users AS su", "p.user_id = su.user_id", "LEFT");
    $this->db->join("tbl_karyawan AS tk", "tk.user_id = p.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("role_id in (4) AND is_terminate =0  AND p.id_client= " . $id_client);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getAnggota($id_spv, &$responseCode)
  {
    $this->db->select("su.user_id, su.email, j.nama_client, j.alamat, su.full_name, tk.tgl_awal_kontrak, tk.tgl_akhir_kontrak, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("tbl_karyawan AS tk", "tk.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("role_id in (5) AND is_terminate =0  AND id_spv= " . $id_spv);

    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPegawaiInputMatrix($role_id, $id_spv, &$responseCode)
  {
    $this->db->select("su.user_id, j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    if ($role_id == 2) { //manager
      $this->db->where("role_id in (3) AND is_terminate =0 AND id_spv= " . $id_spv);
    } else if ($role_id == 3) { //chief
      $this->db->where("role_id in (4) AND is_terminate =0 AND id_spv= " . $id_spv);
    } else if ($role_id == 4) { //danru
      $this->db->where("role_id in (5) AND is_terminate =0  AND id_spv= " . $id_spv);
    }

    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPegawaiApproveMatrix($role_id, $id_spv, &$responseCode)
  {
    $this->db->select("tus.user_id, j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("tbl_user_skill_matrix AS tus");
    $this->db->join("sys_users AS su", "su.user_id = tus.user_id", "LEFT");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    if ($role_id == 6) { //direktur
      $this->db->where("role_id in (3) AND is_terminate =0 AND id_spv IN (select user_id from sys_users where id_spv = " . $id_spv . ")");
    } else if ($role_id == 2) { //manager
      $this->db->where("role_id in (4) AND is_terminate =0 AND id_spv IN (select user_id from sys_users where id_spv = " . $id_spv . ")");
    } else if ($role_id == 3) { //chief
      $this->db->where("role_id in (5) AND is_terminate =0  AND id_spv IN (select user_id from sys_users where id_spv = " . $id_spv . ")");
    }
    $this->db->where("tus.is_approved = 0 AND tus.is_rejected = 0 ");

    $this->db->distinct();
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getHistoryApproveMatrix($role_id, $id_spv, &$responseCode)
  {
    $this->db->select("tus.user_id, j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("tbl_user_skill_matrix AS tus");
    $this->db->join("sys_users AS su", "su.user_id = tus.user_id", "LEFT");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    if ($role_id == 6) { //direktur
      $this->db->where("role_id in (3) AND is_terminate =0 ");
    } else if ($role_id == 2) { //manager
      $this->db->where("role_id in (4) AND is_terminate =0 ");
    } else if ($role_id == 3) { //chief
      $this->db->where("role_id in (5) AND is_terminate =0  AND id_spv IN (select user_id from sys_users where id_spv = " . $id_spv . ")");
    }
    $this->db->where("(tus.is_approved = 1 OR tus.is_rejected = 1) ");

    $this->db->distinct();
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getDetailMatrixById($id, &$responseCode)
  {
    // $this->db->select("sm.nama_skill_matrix, tus.nilai");
    // $this->db->from("tbl_user_skill_matrix AS tus");
    // $this->db->join("ref_skill_matrix AS sm", "sm.id_skill_matrix = tus.id_skill_matrix", "LEFT");
    // $this->db->where("tus.user_id = '$id' ");
    // $this->db->where("tus.is_approved = 1 ");
    // $this->db->group_by("tus.id_skill_matrix");
    // $this->db->order_by("tus.tgl_penilaian DESC");

    $query = $this->db->query("SELECT * FROM (SELECT DISTINCT tus.id_user_skill_matrix,tus.id_skill_matrix,`sm`.`nama_skill_matrix`, `tus`.`nilai`
      FROM `tbl_user_skill_matrix` AS `tus`
      LEFT JOIN `ref_skill_matrix` AS `sm` ON `sm`.`id_skill_matrix` = `tus`.`id_skill_matrix`
      WHERE `tus`.`user_id` = '$id' 
      AND `tus`.`is_approved` = 1
      ORDER BY tus.tgl_penilaian DESC,tus.id_user_skill_matrix DESC
    ) AS tmp_table GROUP BY id_skill_matrix");
    // $query = $this->db->get();
    // print_r($this->db->last_query());
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found',
        'data' => []
      );
      $responseCode = 400;
      return $response;
    }
  }

  public function getEmployeeMatrixById($id, &$responseCode)
  {
    $this->db->select("j.nama_client, j.alamat, su.full_name, su.img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("su.user_id = '$id' ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("sm.nama_skill_matrix, tus.nilai");
      $this->db->from("tbl_user_skill_matrix AS tus");
      $this->db->join("ref_skill_matrix AS sm", "sm.id_skill_matrix = tus.id_skill_matrix", "LEFT");
      $this->db->where("tus.user_id = '$id' ");
      $this->db->where("tus.is_approved = 1 ");
      $this->db->group_by("tus.id_skill_matrix");
      $this->db->order_by("tus.tgl_penilaian DESC");
      $query = $this->db->get();
      $skill_rows = $query->result_array();
      $object = (object) [
        'header' => $rows,
        'detail' => $skill_rows
      ];

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $object
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 400;
      return $response;
    }
  }

  //belum dipake
  public function getPegawaiById($id)
  {
    $this->db->select("j.nama_client, j.alamat, su.full_name, su.img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "INNER");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("su.user_id = '$id' ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 400;
      return $response;
    }
  }

  public function downloadReport($id)
  {
  }
  public function createEmployeeMatrix($data, &$responseCode)
  {
    $arrData = [
      'user_id' => $data->user_id,
      'id_skill_matrix' => $data->id_skill_matrix,
      'nilai' => $data->nilai,
      'tgl_penilaian' => $data->tgl_penilaian,
      // 'tgl_penilaian' => format_date_as_db_format($data->tgl_report),
      'create_at' => getsysdate(),
      'create_by' =>  $data->create_by
    ];
    if ($this->db->insert("tbl_user_skill_matrix", $arrData)) {

      $response = [
        "status" => "success",
        "message" => MSG_INSERT_SUCCESS,
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => MSG_INSERT_FAILED,
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function createEmployeePromosi($data, &$responseCode)
  {
    $arrData = [
      'user_id' => $data->user_id,
      'id_jabatan_sebelum' => $data->id_jabatan_sebelum,
      'id_jabatan_setelah' => $data->id_jabatan_setelah,
      'ket_promosi' => $data->ket_promosi,
      'tgl_promosi' => $data->tgl_promosi,
      'create_at' => getsysdate(),
      'create_by' =>  $data->create_by
    ];
    if ($this->db->insert("tbl_history_promosi", $arrData)) {
      $arrUpdate = [
        'id_jabatan' => $data->id_jabatan_setelah,
        'update_at' => getsysdate(),
        'update_by' =>  $data->create_by
      ];

      $this->db->where('user_id', $data->user_id);
      if ($this->db->update("sys_users", $arrUpdate)) {
        $response = [
          "status" => "success",
          "message" => MSG_INSERT_SUCCESS,
        ];
        $responseCode = 201;
      }
    } else {
      $response = [
        "status" => "error",
        "message" => MSG_INSERT_FAILED,
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function createEmployeeMutasi($data, &$responseCode)
  {
    $arrData = [
      'user_id' => $data->user_id,
      'id_alasan_mutasi' => $data->id_alasan_mutasi,
      'ket_mutasi' => $data->ket_mutasi,
      'tgl_mutasi' => $data->tgl_mutasi,
      'id_client_lama' => $data->id_client_lama,
      'id_client_baru' => $data->id_client_baru,
      'create_at' => getsysdate(),
      'create_by' =>  $data->create_by
    ];
    if ($this->db->insert("tbl_history_mutasi", $arrData)) {
      $arrUpdate = [
        'id_client' => $data->id_client_baru,
        'update_at' => getsysdate(),
        'update_by' =>  $data->create_by
      ];

      $this->db->where('user_id', $data->user_id);
      if ($this->db->update("tbl_penugasan", $arrUpdate)) {

        $arrUpdateSpv = [
          // 'id_spv' => $data->id_spv, blm di set di app=====================================================================================================
          'update_at' => getsysdate(),
          'update_by' =>  $data->create_by
        ];

        $this->db->where('user_id', $data->user_id);
        if ($this->db->update("sys_users", $arrUpdateSpv)) {
          $response = [
            "status" => "success",
            "message" => MSG_INSERT_SUCCESS,
          ];
          $responseCode = 201;
        }
      }
    } else {
      $response = [
        "status" => "error",
        "message" => MSG_INSERT_FAILED,
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function createEmployeeTerminate($data, &$responseCode)
  {
    $arrData = [
      'is_terminate' => 1,
      'id_alasan_resign' => $data->id_alasan_resign,
      'alasan_terminate' => $data->alasan_terminate,
      'tgl_terminate' => $data->tgl_terminate,
      'update_at' => getsysdate(),
      'update_by' =>  $data->update_by
    ];
    $this->db->where('user_id', $data->user_id);
    if ($this->db->update("sys_users", $arrData)) {

      $response = [
        "status" => "success",
        "message" => MSG_UPDATE_SUCCESS,
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => MSG_UPDATE_FAILED,
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getListApprovalEmployeeMatrixById($id, &$responseCode)
  {
    $this->db->select("su.user_id, j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("su.user_id = '$id' ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("j.nama_skill_matrix, tus.id_user_skill_matrix");
      $this->db->from("tbl_user_skill_matrix AS tus");
      $this->db->join("ref_skill_matrix AS j", "j.id_skill_matrix = tus.id_skill_matrix", "LEFT");
      $this->db->where("tus.user_id = '$id' AND is_approved= 0 AND is_rejected=0");
      $this->db->group_by("tus.id_skill_matrix");
      $this->db->order_by("tus.tgl_penilaian DESC");
      $query_dtl = $this->db->get();
      $skill_rows = $query_dtl->result_array();
      $object = (object) [
        'header' => $rows,
        'detail' => $skill_rows
      ];

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $object
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListHistoryApprovalEmployeeMatrixById($id, &$responseCode)
  {
    $this->db->select("su.user_id, j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("su.user_id = '$id' ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("j.nama_skill_matrix, su.id_user_skill_matrix");
      $this->db->from("tbl_user_skill_matrix AS su");
      $this->db->join("ref_skill_matrix AS j", "j.id_skill_matrix = su.id_skill_matrix", "LEFT");
      $this->db->where("su.user_id = '$id' AND (is_approved= 1 OR is_rejected=1)");
      $query_dtl = $this->db->get();
      $skill_rows = $query_dtl->result_array();
      $object = (object) [
        'header' => $rows,
        'detail' => $skill_rows
      ];

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $object
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getApprovalEmployeeMatrixById($id, $id_user_skill_matrix, &$responseCode)
  {
    $this->db->select("j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("sys_users AS su");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("su.user_id = '$id' ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("su.id_user_skill_matrix, j.nama_skill_matrix, su.nilai, su.tgl_penilaian, su.is_approved, su.approve_date, su.is_rejected, su.reject_date");
      $this->db->from("tbl_user_skill_matrix AS su");
      $this->db->join("ref_skill_matrix AS j", "j.id_skill_matrix = su.id_skill_matrix", "LEFT");
      $this->db->where("su.id_user_skill_matrix = '$id_user_skill_matrix'");
      $query = $this->db->get();
      $skill_rows = $query->row();
      $object = (object) [
        'header' => $rows,
        'detail' => $skill_rows
      ];

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $object
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 400;
      return $response;
    }
  }

  public function createApprovalEmployeeMatrix($data, &$responseCode)
  {
    if ($data->is_approved == 1) {
      $arrData = [
        'is_approved' => $data->is_approved,
        'is_rejected' => $data->is_rejected,
        'approve_date' => getsysdate(),
        'approve_by' =>  $data->create_by
      ];
    } else {
      $arrData = [
        'is_rejected' => $data->is_rejected,
        'is_approved' => $data->is_approved,
        'reject_date' => getsysdate(),
        'reject_by' =>  $data->create_by
      ];
    }

    $this->db->where('id_user_skill_matrix = ' . $data->id_user_skill_matrix);
    if ($this->db->update("tbl_user_skill_matrix", $arrData)) {

      $response = [
        "status" => "success",
        "message" => "Penilaian berhasil diupdate",
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => MSG_UPDATE_FAILED,
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function createLogBook($data_img, &$responseCode)
  {
    if (move_uploaded_file($data_img['tmp_name'], './img_logbook/' . $data_img['filename'])) {
      $source_path = FCPATH . 'img_logbook/';
      $target_path = FCPATH . 'img_logbook/';

      if ($data_img["size"] > 36627) {
        resizeImage($data_img['filename'], $source_path, $target_path);
      }
      $wkt = json_decode($this->input->post('waktu_kejadian'));
      $tgl = json_decode($this->input->post('tgl_kejadian'));
      $arrData = [
        'tgl_kejadian' => $tgl,
        'waktu_kejadian' => $wkt,
        'keterangan' => $this->input->post('keterangan'),
        'detail_kejadian' => $this->input->post('detail_kejadian'),
        'lokasi_kejadian' => $this->input->post('lokasi_kejadian'),
        'img_logbook' => $data_img['filename'],
        'create_at' => getsysdate(),
        'create_by' =>  $this->input->post('user_id'),
      ];
      if ($this->db->insert("tbl_log_book", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Log Book berhasil dibuat',
          "url" => $data_img['urlgambar']
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Log Book gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $response = array(
        "status" => "error",
        "error" => true,
        "message" => "Error uploading the file!"
      );
      $responseCode = 400;
    }

    return $response;
  }

  public function getHistoryLogBook($user_id, $role_id = "", &$responseCode)
  {
    if ($role_id == 3) { //chief
      $this->db->select("tb.id_log_book, tb.tgl_kejadian, tb.waktu_kejadian, tb.detail_kejadian, lokasi_kejadian, keterangan, concat('" . HOSTNAMEAPI . "/img_logbook/',tb.img_logbook) as img_logbook");
      $this->db->from("tbl_log_book as tb");
      $this->db->join("sys_users AS su", "tb.create_by = su.user_id", "LEFT");
      $this->db->where("(su.id_spv = $user_id AND su.role_id = 4)"); //yg diambil hanya logbook danru
      $this->db->order_by("tb.create_at DESC ");
    } else {
      $this->db->select("tb.id_log_book, tb.tgl_kejadian, tb.waktu_kejadian, tb.detail_kejadian, lokasi_kejadian, keterangan, concat('" . HOSTNAMEAPI . "/img_logbook/',tb.img_logbook) as img_logbook");
      $this->db->from("tbl_log_book as tb");
      $this->db->where("create_by = $user_id ");
      $this->db->order_by("tb.create_at DESC ");
    }

    $query = $this->db->get();
    // echo $this->db->last_query(); die;
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getHistoryLogBookById($id, &$responseCode)
  {
    $this->db->select("tb.id_log_book, tb.tgl_kejadian, tb.waktu_kejadian, tb.detail_kejadian, lokasi_kejadian, keterangan, concat('" . HOSTNAMEAPI . "/img_logbook/',tb.img_logbook) as img_logbook");
    $this->db->from("tbl_log_book as tb");
    $this->db->where("id_log_book = $id ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 400;
      return $response;
    }
  }

  public function getDanruLogBookTeam($user_id, &$responseCode)
  {

    $this->db->select("tb.id_log_book, su.user_id, j.nama_client, j.alamat, su.full_name, concat('" . HOSTNAMEAPI . "/photos/',su.img_profile) as img_profile");
    $this->db->from("tbl_log_book as tb");
    $this->db->join("sys_users AS su", "su.user_id = tb.create_by", "LEFT");
    $this->db->join("tbl_penugasan AS p", "p.user_id = su.user_id", "LEFT");
    $this->db->join("ref_client AS j", "j.id_client = p.id_client", "LEFT");
    $this->db->where("su.id_spv = '$user_id' AND su.is_terminate = 0 ");
    $this->db->order_by("tb.create_at DESC ");

    $query = $this->db->get();
    // echo $this->db->last_query(); die;
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
}
