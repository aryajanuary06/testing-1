<?php

// extends class Model
class Penjualan_model extends CI_Model
{

  public function getListPenjualan($user_id, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_penjualan");
    $this->db->where("create_by= " . $user_id);
    $this->db->order_by("id_penjualan DESC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPenjualanById($id_penjualan, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_penjualan");
    $this->db->where("id_penjualan= " . $id_penjualan);
    $this->db->order_by('id_penjualan DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("*");
      $this->db->from("tbl_penjualan_dtl");
      $this->db->where("id_penjualan= " . $id_penjualan);
      $this->db->order_by('name DESC');

      $query2 = $this->db->get();
      $penjualan_dtl = $query2->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        'penjualan_dtl' => $penjualan_dtl
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function search($data, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_penjualan");
    $this->db->where("user_id= " . $data->user_id);
    if (!empty($data->keywords)) {
      $this->db->where("tanggal", $data->keywords);
    }
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createPenjualan($data, &$responseCode)
  {
    date_default_timezone_set("Asia/Jakarta");
    $date = new DateTime($data->tanggal);

    // return $data;
    if ($data->id_penjualan == "") {
      $arrData = [
        'tanggal' => $date->format('Y-m-d'),
        'create_by' => $data->user_id,
        'create_at' => getsysdate(),
      ];
      if ($this->db->insert("tbl_penjualan", $arrData)) {
        $ls = array();
        $i = 0;
        foreach ($data->listDtl as $value) {
          $ls[$i]['id_penjualan'] = $this->db->insert_id();
          $ls[$i]['id_penjualan_dtl'] = $value->id_penjualan_dtl;
          $ls[$i]['id_product'] = $value->id_product;
          $ls[$i]['name'] = $value->name;
          $ls[$i]['price'] = $value->price;
          $ls[$i]['qty'] = $value->qty;
          $i++;
        }
        $this->db->insert_batch('tbl_penjualan_dtl', $ls);

        $response = [
          "status" => "success",
          "message" => 'Data berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $arrData = [
        'tanggal' => $date->format('Y-m-d'),
        'update_by' => $data->user_id,
        'update_at' => getsysdate(),
      ];
      $this->db->where('id_penjualan', $data->id_penjualan);
      if ($this->db->update("tbl_penjualan", $arrData)) {
        $this->db->where("id_penjualan", $data->id_penjualan);
        $this->db->delete("tbl_penjualan_dtl");
        $ls = array();
        $i = 0;
        foreach ($data->listDtl as $value) {
          $ls[$i]['id_penjualan'] = $data->id_penjualan;
          $ls[$i]['id_penjualan_dtl'] = $value->id_penjualan_dtl;
          $ls[$i]['id_product'] = $value->id_product;
          $ls[$i]['name'] = $value->name;
          $ls[$i]['price'] = $value->price;
          $ls[$i]['qty'] = $value->qty;
          $i++;
        }
        $this->db->insert_batch('tbl_penjualan_dtl', $ls);
  
        $response = [
          "status" => "success",
          "message" => 'Data berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal diupdate',
        ];
        $responseCode = 404;
      }
    }
    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_penjualan", $data->id);
    if ($this->db->delete("tbl_penjualan")) {
      $this->db->where("id_penjualan", $data->id);
      $this->db->delete("tbl_penjualan_dtl");
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function updateShowReport($data, &$responseCode)
  {
    $nilai = $data->value ? 1 : 0;
    $arrData = [
      'is_show_sales_report' => $nilai,
      'update_by' => $data->user_id,
      'update_at' => getsysdate(),
    ];
  $this->db->where("user_id", $data->user_id);
    if ($this->db->update("sys_users", $arrData)) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil diupdate',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal diupdate',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getProduct($user_id, &$responseCode)
  {
    $this->db->select("id_product, name, price");
    $this->db->from("tbl_product");
    $this->db->where("user_id = $user_id");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
}
