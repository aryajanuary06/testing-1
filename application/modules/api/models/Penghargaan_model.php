<?php

// extends class Model
class Penghargaan_model extends CI_Model
{

  public function getListPenghargaan($user_id, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_penghargaan/',img) as img_penghargaan");
    $this->db->from("tbl_penghargaan");
    $this->db->where("user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPenghargaanById($id_penghargaan, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_penghargaan/',img) as img_penghargaan");
    $this->db->from("tbl_penghargaan");
    $this->db->where("id_penghargaan= " . $id_penghargaan);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function search($data, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_penghargaan/',img) as img_penghargaan");
    $this->db->from("tbl_penghargaan");
    $this->db->where("user_id= " . $data->user_id);
    if (!empty($data->keywords)) {
      $this->db->or_like("title", $data->keywords);
    }
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createPenghargaan($data_img, &$responseCode)
  {
    if (count($data_img) > 0) {
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_penghargaan/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_penghargaan/';
        $target_path = FCPATH . 'assets/images/img_penghargaan/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
        $arrData = [
          'user_id' => $this->input->post('user_id'),
          'title' => $this->input->post('title'),
          'desc' => $this->input->post('desc'),
          'img' => $data_img['filename'],
          'create_at' => getsysdate(),
        ];
      }
    } else {
      $arrData = [
        'user_id' => $this->input->post('user_id'),
        'title' => $this->input->post('title'),
        'desc' => $this->input->post('desc'),
        'create_at' => getsysdate(),
      ];
    }

    if ($this->input->post('id_penghargaan') == "") {
      if ($this->db->insert("tbl_penghargaan", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'penghargaan berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'penghargaan gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_penghargaan', $this->input->post('id_penghargaan'));
      if ($this->db->update("tbl_penghargaan", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'penghargaan berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'penghargaan gagal diupdate',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function createAspirasi($data_img, &$responseCode)
  {
    if (count($data_img) > 0) {
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_aspirasi/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_aspirasi/';
        $target_path = FCPATH . 'assets/images/img_aspirasi/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
        $arrData = [
          'user_id' => $this->input->post('user_id'),
          'title' => $this->input->post('title'),
          'desc' => $this->input->post('desc'),
          'kontak' => $this->input->post('kontak'),
          'img' => $data_img['filename'],
          'create_at' => getsysdate(),
        ];
      }
    } else {
      $arrData = [
        'user_id' => $this->input->post('user_id'),
        'title' => $this->input->post('title'),
        'desc' => $this->input->post('desc'),
        'kontak' => $this->input->post('kontak'),
        'create_at' => getsysdate(),
      ];
    }

    if ($this->input->post('id_aspirasi') == "") {
      if ($this->db->insert("tbl_aspirasi", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'aspirasi berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'aspirasi gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_aspirasi', $this->input->post('id_aspirasi'));
      if ($this->db->update("tbl_aspirasi", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'aspirasi berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'aspirasi gagal diupdate',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_penghargaan", $data->id);
    if ($this->db->delete("tbl_penghargaan")) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }
}
