<?php

// extends class Model
class Artikel_model extends CI_Model
{

  public function getArtikel($id_kat_posts, &$responseCode)
  {
    $this->db->select("*, concat('" . HOSTNAME . "/img_posts/', img) as img_posts");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts = '$id_kat_posts' ");
    $this->db->order_by("create_at DESC ");
    $query = $this->db->get();
    // echo $this->db->last_query(); die;
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getLastArtikel($id_kat_posts, &$responseCode)
  {
    $this->db->select("*, concat('" . HOSTNAME . "/img_posts/', img) as img_posts");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts = '$id_kat_posts' ");
    $this->db->order_by("create_at DESC ");
    $this->db->limit(1);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getArtikelById($id, &$responseCode)
  {
    $this->db->select("*, concat('" . HOSTNAME . "/img_posts/', img) as img_posts");
    $this->db->from("tbl_posts");
    $this->db->where("id_posts = '$id' ");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 400;
      return $response;
    }
  }
}
