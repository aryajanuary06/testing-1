<?php

// extends class Model
class Ide_startup_model extends CI_Model
{

  public function getListIde_startup(&$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_ide_startup");
    $this->db->order_by("id_ide_startup DESC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getIde_startupById($id_ide_startup, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_ide_startup");
    $this->db->where("id_ide_startup= " . $id_ide_startup);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function search($data, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_ide_startup");
    if (!empty($data->keywords)) {
      $this->db->where("tanggal", $data->keywords);
    }
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createIde_startup($data, &$responseCode)
  {
    date_default_timezone_set("Asia/Jakarta");
    $arrData = [
      'title' => $data->title,
      'desc' => $data->desc,
      'name' => $data->name,
      'umkm' => $data->umkm,
      'tanggal' => getsysdate(),
      'create_at' => getsysdate(),
    ];
    if ($this->db->insert("tbl_ide_startup", $arrData)) {

      $response = [
        "status" => "success",
        "message" => 'Data berhasil dibuat',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dibuat',
      ];
      $responseCode = 404;
    }
    return $response;
  }
}
