<?php

// extends class Model
class Umkm_model extends CI_Model
{

  public function getListUmkm($offset, $search, &$responseCode)
  {
    // $like = "";
    // if ($data->filter != "rahasia") {
    //   $like = "";
    // }
    // return $like;
    
    $this->db->select("tu.id_umkm, tu.user_id, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',img) as img_umkm, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',su.img_umkm_pp) as img_umkm_pp");
    $this->db->from("sys_users su");
    $this->db->join("tbl_umkm tu", "su.user_id=tu.user_id", "inner");
    $this->db->where("role_id= 2 AND tu.title LIKE '%".$search."%'");
    $this->db->order_by("tu.id_umkm DESC");
    $this->db->limit(10, $offset);
    // $this->db->limit(10, $offset*10-10);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found ',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found ',
        'data' => []
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function search($data, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',img) as img_umkm, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',img_pp) as img_photo_profile");
    $this->db->from("tbl_umkm");
    if (!empty($data->keywords)) {
      $this->db->or_like("title", $data->keywords);
    }

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getEditUmkmById($user_id, &$responseCode)
  {
    $this->db->select("tu.id_umkm, tu.title, tu.telp, tu.alamat, tu.id_provinsi, tu.id_kabupaten, tu.id_kecamatan, tu.id_kelurahan, tu.id_kat_umkm, tu.sejarah, su.no_telp, su.full_name, su.email, su.username, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm
    , concat('" . HOSTNAMEAPI . "/assets/images/img_profile/',su.img_profile) as img_profile, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',su.img_umkm_pp) as img_umkm_pp");
    $this->db->from("tbl_umkm tu");
    $this->db->join("sys_users su", "su.user_id=tu.user_id", "LEFT");
    $this->db->where("tu.user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getUmkmById($user_id, &$responseCode)
  {
    $this->db->select("su.is_show_sales_report, tu.id_umkm, tu.title, tu.sejarah, tku.name as kat_umkm, tu.alamat, rp.nama_provinsi, rk.nama_kabupaten, rc.nama_kecamatan, rl.nama_kelurahan, su.full_name, su.no_telp, tu.telp, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',su.img_umkm_pp) as img_umkm_pp");
    $this->db->from("tbl_umkm tu");
    $this->db->join("sys_users su", "su.user_id=tu.user_id", "LEFT");
    $this->db->join("tbl_kat_umkm AS tku", "tku.id_kat_umkm = tu.id_kat_umkm", "LEFT");
    $this->db->join("ref_provinsi AS rp", "tu.id_provinsi = rp.id_provinsi", "LEFT");
    $this->db->join("ref_kabupaten AS rk", "tu.id_kabupaten = rk.id_kabupaten", "LEFT");
    $this->db->join("ref_kecamatan AS rc", "tu.id_kecamatan = rc.id_kecamatan", "LEFT");
    $this->db->join("ref_kelurahan AS rl", "tu.id_kelurahan = rl.id_kelurahan", "LEFT");
    $this->db->where("tu.user_id= " . $user_id);

    $query = $this->db->get();

    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("*, concat('" . HOSTNAMEAPI . "/assets/images/img_penghargaan/',tp.img) as img_penghargaan");
      $this->db->from("tbl_penghargaan tp");
      $this->db->where("user_id= " . $user_id);
      $this->db->order_by('id_penghargaan DESC');
      $query2 = $this->db->get();
      $penghargaan = $query2->result_array();

      $this->db->select("*, concat('" . HOSTNAMEAPI . "/assets/images/img_sertifikasi/',tp.img) as img_sertifikasi");
      $this->db->from("tbl_sertifikasi tp");
      $this->db->where("user_id= " . $user_id);
      $this->db->order_by('id_sertifikasi DESC');
      $query3 = $this->db->get();
      $sertifikasi = $query3->result_array();

      // $this->db->select("DATE_FORMAT(tanggal, '%d %M %Y') AS tanggal,SUM(kapasitas_prod) AS kapasitas_prod");
      $this->db->select("DATE_FORMAT(tanggal, '%M %Y') AS tanggal,SUM(kapasitas_prod) AS kapasitas_prod");
      $this->db->from("tbl_kapasitas_prod");
      $this->db->where("user_id= " . $user_id);
      $this->db->group_by('year(tanggal),month(tanggal)');
      $this->db->order_by('tanggal DESC');
      $query4 = $this->db->get();
      // echo "<pre>"; print_r($query4); echo "</pre>"; die();
      $kapasitas = ( $query4 !== FALSE && $query4->num_rows() > 0 ) ? $query4->result_array() : [];
      
      $this->db->select("DATE_FORMAT(tanggal, '%M %Y') AS tanggal,SUM(permintaan_prod) AS permintaan_prod");
      $this->db->from("tbl_permintaan_prod");
      $this->db->where("user_id= " . $user_id);
      $this->db->group_by('year(tanggal),month(tanggal)');
      $this->db->order_by('tanggal DESC');
      $query5 = $this->db->get();
      $permintaan = ( $query5 !== FALSE && $query5->num_rows() > 0 ) ? $query5->result_array() : [];

      $this->db->select("id_product, name, price, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',img) as img_product");
      $this->db->from("tbl_product");
      $this->db->where("user_id= " . $user_id);
      $this->db->order_by("id_product DESC");
      $query6 = $this->db->get();
      $product = ( $query6 !== FALSE && $query6->num_rows() > 0 ) ? $query6->result_array() : [];

      $this->db->select("tbb.name as name_bb, tkbb.name as name_kat_bb, desc");
      $this->db->from("tbl_bahan_baku tbb");
      $this->db->join("tbl_kat_bahan_baku tkbb", "tbb.id_kat_bahan_baku=tkbb.id_kat_bahan_baku", "LEFT");
      $this->db->where("tbb.user_id= " . $user_id);
      // $this->db->group_by('tbb.name, tbb.id_kat_bahan_baku,desc');
      $this->db->order_by('bulan DESC');
      $query7 = $this->db->get();
      // return $this->db->last_query();
      $bb = ( $query7 !== FALSE && $query7->num_rows() > 0 ) ? $query7->result_array() : [];

      $this->db->select("name, desc, SUM(jml) AS jml");
      $this->db->from("tbl_bahan_baku");
      $this->db->where("user_id= " . $user_id);
      $this->db->group_by('name, id_kat_bahan_baku');
      $this->db->order_by('bulan DESC');
      $query8 = $this->db->get();
      $kebutuhan_bb = ( $query8 !== FALSE && $query8->num_rows() > 0 ) ? $query8->result_array() : [];

      $this->db->select("id_workshop, lokasi, concat('" . HOSTNAMEAPI . "/assets/images/img_workshop/',img) as img");
      $this->db->from("tbl_workshop");
      $this->db->where("user_id= " . $user_id . " AND is_temp = 0");
      $this->db->order_by("id_workshop DESC");
      $query9 = $this->db->get();
      $workshop = ( $query9 !== FALSE && $query9->num_rows() > 0 ) ? $query9->result_array() : [];

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        'penghargaan' => $penghargaan,
        'sertifikasi' => $sertifikasi,
        'kapasitas' => $kapasitas,
        'permintaan' => $permintaan,
        'product' => $product,
        'bb' => $bb,
        'kebutuhan_bb' => $kebutuhan_bb,
        'workshop' => $workshop,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getCountUmkmById($user_id, &$responseCode)
  {
    $this->db->select("count(id_product) as c");
    $this->db->from("tbl_product tp");
    $this->db->where("user_id= " . $user_id);
    $query = $this->db->get();
    $produk = $query->row();

    $this->db->select("count(id_penghargaan) as c");
    $this->db->from("tbl_penghargaan tp");
    $this->db->where("user_id= " . $user_id);
    $query2 = $this->db->get();
    $penghargaan = $query2->row();

    $this->db->select("count(id_sertifikasi) as c");
    $this->db->from("tbl_sertifikasi tp");
    $this->db->where("user_id= " . $user_id);
    $query3 = $this->db->get();
    $sertifikasi = $query3->row();


    $response = array(
      'status' => 'success',
      'message' => 'Data Found',
      'produk' => $produk->c,
      'penghargaan' => $penghargaan->c,
      'sertifikasi' => $sertifikasi->c,
    );
    $responseCode = 200;
    return $response;
  }

  public function getBbById($data, &$responseCode)
  {
    $this->db->select("tbb.name as name_bb, tkbb.name as name_kat_bb, desc");
    $this->db->from("tbl_bahan_baku tbb");
    $this->db->join("tbl_kat_bahan_baku tkbb", "tbb.id_kat_bahan_baku=tkbb.id_kat_bahan_baku", "LEFT");
    $this->db->where("tbb.user_id= " . $data->user_id);
    $this->db->group_by('tbb.name, tbb.id_kat_bahan_baku,desc');
    $this->db->order_by('bulan DESC');
    $query7 = $this->db->get();
    $bb = ( $query7 !== FALSE && $query7->num_rows() > 0 ) ? $query7->result_array() : [];

    $this->db->select("name, desc, SUM(jml) AS jml, ss.value_setting");
    $this->db->from("tbl_bahan_baku");
    $this->db->join("sys_settings ss", "ss.id_setting = tbl_bahan_baku.id_satuan_bb", "LEFT");
    $this->db->where("user_id= " . $data->user_id);
    $this->db->where("ss.category_setting = 'satuan_bb'");
    $this->db->where("MONTH(bulan)= MONTH('" . $data->month . "') AND YEAR(bulan) = YEAR('".$data->month."')");
    $this->db->group_by('name, id_kat_bahan_baku, year(bulan), month(bulan)');
    $this->db->order_by('bulan DESC');
    $query4 = $this->db->get();
    // return $this->db->last_query();
    $kebutuhan_bb =  ( $query4 !== FALSE && $query4->num_rows() > 0 ) ? $query4->result_array() : [];

    $response = array(
      'status' => 'success',
      'message' => $this->db->last_query(),
      'bb' => $bb,
      'kebutuhan_bb' => $kebutuhan_bb,
    );
    $responseCode = 200;
    return $response;
  }

  public function getProduksiById($data, &$responseCode)
  {
    if ($data->filter == 'bulan') {
      $this->db->select("DATE_FORMAT(tanggal, '%M %Y') AS tanggal,SUM(permintaan_prod) AS permintaan_prod");
      $this->db->from("tbl_permintaan_prod");
      $this->db->where("user_id= " . $data->user_id);
      $this->db->where("tanggal BETWEEN '" . $data->date . "' and '" . $data->date2 . "'");
      $this->db->group_by('year(tanggal),month(tanggal)');
      $this->db->order_by('tanggal DESC');
      $query5 = $this->db->get();
      $permintaan = $query5->result_array();

      $this->db->select("DATE_FORMAT(tanggal, '%M %Y') AS tanggal,SUM(kapasitas_prod) AS kapasitas_prod");
      $this->db->from("tbl_kapasitas_prod");
      $this->db->where("user_id= " . $data->user_id);
      $this->db->where("tanggal BETWEEN '" . $data->date . "' and '" . $data->date2 . "'");
      $this->db->group_by('year(tanggal),month(tanggal)');
      $this->db->order_by('tanggal DESC');
      $query4 = $this->db->get();
      $kapasitas = $query4->result_array();
    } else {
      $this->db->select("DATE_FORMAT(tanggal, '%Y') AS tanggal,SUM(permintaan_prod) AS permintaan_prod");
      $this->db->from("tbl_permintaan_prod");
      $this->db->where("user_id= " . $data->user_id);
      $this->db->where("tanggal BETWEEN '" . $data->date . "' and '" . $data->date2 . "'");
      $this->db->group_by('year(tanggal)');
      $this->db->order_by('tanggal DESC');
      $query5 = $this->db->get();
      $permintaan = $query5->result_array();

      $this->db->select("DATE_FORMAT(tanggal, '%Y') AS tanggal,SUM(kapasitas_prod) AS kapasitas_prod");
      $this->db->from("tbl_kapasitas_prod");
      $this->db->where("user_id= " . $data->user_id);
      $this->db->where("tanggal BETWEEN '" . $data->date . "' and '" . $data->date2 . "'");
      // $this->db->where("tanggal >= '$data->date' AND tanggal < '$data->date2' + interval 1 day ");
      $this->db->group_by('year(tanggal)');
      $this->db->order_by('tanggal DESC');
      $query4 = $this->db->get();
      // echo $this->db->last_query();
      $kapasitas = $query4->result_array();
    }

    $response = array(
      'status' => 'success',
      'message' => $this->db->last_query(),
      'kapasitas' => $kapasitas,
      'permintaan' => $permintaan,
    );
    $responseCode = 200;
    return $response;
  }

  public function getPenjualanById($data, &$responseCode)
  {
    // $n = date('Y-m-d', strtotime( $data->date . " -1 days"));
    $date1 = date('Y-m-d', strtotime( $data->date));
    $date2 = date('Y-m-d', strtotime( $data->date2));
    if ($data->filter == 'bulan') {
      $this->db->select("tpd.name, DATE_FORMAT(tp.tanggal, '%M %Y') AS tanggal,SUM(tpd.qty) AS qty");
      $this->db->from("tbl_penjualan tp");
      $this->db->join("tbl_penjualan_dtl tpd", "tp.id_penjualan=tpd.id_penjualan", "LEFT");
      $this->db->where("tp.create_by= " . $data->user_id);
      $this->db->where("tp.tanggal BETWEEN '" . $date1 . "' and '" . $date2 . "' AND tpd.id_penjualan IS NOT NULL");
      $this->db->group_by('tpd.id_product, year(tp.tanggal),month(tp.tanggal)');
      $this->db->order_by('tp.tanggal DESC');
      $query4 = $this->db->get();
      $res = $query4->result_array();
    } else {
      $this->db->select("tpd.name, DATE_FORMAT(tp.tanggal, '%d %M %Y') AS tanggal,SUM(tpd.qty) AS qty");
      $this->db->from("tbl_penjualan tp");
      $this->db->join("tbl_penjualan_dtl tpd", "tp.id_penjualan=tpd.id_penjualan", "LEFT");
      $this->db->where("tp.create_by= " . $data->user_id);
      $this->db->where("tp.tanggal BETWEEN '" . $date1 . "' and '" . $date2 . "' AND tpd.id_penjualan IS NOT NULL");
      $this->db->group_by('tpd.id_product, tp.tanggal');
      $this->db->order_by('tp.tanggal DESC');
      $query4 = $this->db->get();
      $res = $query4->result_array();
    }

    $response = array(
      'status' => 'success',
      'message' => $this->db->last_query(),
      'data' => $res,
    );
    $responseCode = 200;
    return $response;
  }

  public function createUmkm($data_img, &$responseCode)
  {
    if (count($data_img) > 0) {
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_umkm/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_umkm/';
        $target_path = FCPATH . 'assets/images/img_umkm/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
        $arrData = [
          'user_id' => $this->input->post('user_id'),
          'title' => $this->input->post('title'),
          'telp' => $this->input->post('telp'),
          'alamat' => $this->input->post('alamat'),
          'id_provinsi' => $this->input->post('id_provinsi'),
          'id_kabupaten' => $this->input->post('id_kabupaten'),
          'id_kecamatan' => $this->input->post('id_kecamatan'),
          'id_kelurahan' => $this->input->post('id_kelurahan'),
          'id_kat_umkm' => $this->input->post('id_kat_umkm'),
          'sejarah' => $this->input->post('sejarah'),
          'img' => $data_img['filename'],
          'create_at' => getsysdate(),
        ];
      }
    } else {
      $arrData = [
        'user_id' => $this->input->post('user_id'),
        'title' => $this->input->post('title'),
        'telp' => $this->input->post('telp'),
        'alamat' => $this->input->post('alamat'),
        'id_provinsi' => $this->input->post('id_provinsi'),
        'id_kabupaten' => $this->input->post('id_kabupaten'),
        'id_kecamatan' => $this->input->post('id_kecamatan'),
        'id_kelurahan' => $this->input->post('id_kelurahan'),
        'id_kat_umkm' => $this->input->post('id_kat_umkm'),
        'sejarah' => $this->input->post('sejarah'),
        'create_at' => getsysdate(),
      ];
    }

    if ($this->input->post('id_umkm') == "") {

      // check user id has umkm.

      $this->db->select("user_id");
      $this->db->from("tbl_umkm");
      $this->db->where("user_id= ", $this->input->post('user_id'));
      $querycheck = $this->db->get();

      if($querycheck ->num_rows() == 0) {
        
        if ($this->db->insert("tbl_umkm", $arrData)) {

          $response = [
            "status" => "success",
            "message" => 'Berhasil',
          ];
          $responseCode = 201;
        } else {
          $response = [
            "status" => "error",
            "message" => 'Gagal',
          ];
          $responseCode = 404;
        }

      } else {
        $response = [
          "status" => "succecc",
          "message" => 'Berhasil',
        ];
        $responseCode = 201;
      }

      
    } else {
      $this->db->where('id_umkm', $this->input->post('id_umkm'));
      if ($this->db->update("tbl_umkm", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Berhasil',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Gagal',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function getTopten($data, &$responseCode)
  {

    if ($data->filter == "all") {
      $this->db->select("tu.id_umkm, tu.user_id, tu.title, tu.sejarah, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("sys_users su");
      $this->db->join("tbl_umkm tu", "su.user_id=tu.user_id", "inner");
      $this->db->where("role_id= 2");
    } else if ($data->filter == "omzet") {
      $this->db->select("tu.id_umkm, tu.user_id, tu.title, tu.sejarah, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("sys_users su");
      $this->db->join("tbl_umkm tu", "su.user_id=tu.user_id", "inner");
      $this->db->where("role_id= 2");
    } else if ($data->filter == "penghargaan") {
      $this->db->select("COUNT(su.user_id) AS jml, tu.id_umkm, tu.user_id, tu.title, tu.sejarah, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("tbl_penghargaan su");
      $this->db->join("tbl_umkm tu", "su.user_id=tu.user_id", "inner");
      // $this->db->where("role_id= 2");
      $this->db->group_by("su.user_id");
      $this->db->order_by("jml DESC");
    } else {
      $this->db->select("COUNT(su.user_id) AS jml, tu.id_umkm, tu.user_id, tu.title, tu.sejarah, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
      $this->db->from("tbl_sertifikasi su");
      $this->db->join("tbl_umkm tu", "su.user_id=tu.user_id", "inner");
      // $this->db->where("role_id= 2");
      $this->db->group_by("su.user_id");
      $this->db->order_by("jml DESC");
    }
    if ($data->kategori != 'all') {
      $this->db->where("id_kat_umkm", $data->kategori);
    }
    if ($data->wilayah != 'all') {
      $this->db->where("id_kabupaten", $data->wilayah);
    }
    $this->db->limit(10);

    $query = $this->db->get();
    // return $this->db->last_query();
    // if ($query->num_rows() > 0) {
    $rows = $query->result_array();
    $response = array(
      'status' => 'success',
      'message' => 'Query berhasil',
      'data' => $rows
    );
    $responseCode = 200;
    return $response;
    // } else {
    //   $response = array(
    //     'status' => 'error',
    //     'message' => 'Data Not Found'
    //   );
    //   $responseCode = 200;
    //   return $response;
    // }
  }

  public function getKatUMKM(&$responseCode)
  {
    $this->db->select("id_kat_umkm, name");
    $this->db->from("tbl_kat_umkm");
    $this->db->order_by("name ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListProvinsi(&$responseCode)
  {
    $this->db->select("id_provinsi, nama_provinsi");
    $this->db->from("ref_provinsi");
    $this->db->order_by("nama_provinsi ASC");
    // $this->db->select("id_kabupaten as id_provinsi, nama_kabupaten as nama_provinsi");
    // $this->db->from("ref_kabupaten");
    // $this->db->where("id_provinsi = 32");
    // $this->db->order_by("nama_kabupaten ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListKabByProvId($id_provinsi, &$responseCode)
  {
    $this->db->select("id_kabupaten, nama_kabupaten");
    $this->db->from("ref_kabupaten");
    $this->db->where("id_provinsi= " . $id_provinsi);
    $this->db->order_by("id_kabupaten ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListKecByKabId($id_kabupaten, &$responseCode)
  {
    $this->db->select("id_kecamatan, nama_kecamatan");
    $this->db->from("ref_kecamatan");
    $this->db->where("id_kabupaten= " . $id_kabupaten);
    $this->db->order_by("id_kecamatan ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListKelByKecId($id_kecamatan, &$responseCode)
  {
    $this->db->select("id_kelurahan, nama_kelurahan");
    $this->db->from("ref_kelurahan");
    $this->db->where("id_kecamatan= " . $id_kecamatan);
    $this->db->order_by("id_kelurahan ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getWorkshopById($id_workshop, &$responseCode)
  {
    $this->db->select("twd.nama, twd.kapasitas, tkaw.name as kat_name, concat('" . HOSTNAMEAPI . "/assets/images/img_alat_workshop/',twd.img) as img_workshop");
    $this->db->from("tbl_workshop_dtl As twd");
    $this->db->join("tbl_kat_alat_workshop tkaw", "tkaw.id_kat_alat_workshop=twd.id_kat_alat_workshop", "LEFT");
    $this->db->where("twd.id_workshop= " . $id_workshop . " AND twd.is_temp = 0");
    $this->db->order_by("twd.id_workshop_dtl ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
  
}
