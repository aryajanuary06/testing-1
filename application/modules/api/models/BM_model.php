<?php

// extends class Model
class BM_model extends CI_Model
{

  public function getListBB($user_id, &$responseCode)
  {
    $this->db->select("name");
    $this->db->from("tbl_kat_bahan_baku");
    $this->db->group_by("name");
    $this->db->order_by("name ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      // $this->db->select("count(id_order_bb) as jml");
      // $this->db->from("tbl_order_bb");
      // $this->db->where("id_penjual = '".$user_id."'");
      // $query2 = $this->db->get();
      // $count_permintaan = $query2->row();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        // 'count_permintaan' => $count_permintaan
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getCountPenjual($user_id, &$responseCode)
  {
    $this->db->select("count(id_order_bb) as jml");
    $this->db->from("tbl_order_bb");
    $this->db->where("id_penjual = '".$user_id."'");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getCountPembeli($user_id, &$responseCode)
  {
    $this->db->select("count(id_order_bb) as jml");
    $this->db->from("tbl_order_bb");
    $this->db->where("id_pembeli = '".$user_id."'");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getUmkmByBB($bahan_baku, $user_id, &$responseCode)
  {
    $this->db->select("tu.id_umkm, tu.user_id, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',su.img_umkm_pp) as img_umkm_pp");
    $this->db->from("tbl_bahan_baku tbb");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tbb.user_id", "LEFT");
    $this->db->join("sys_users su", "su.user_id=tu.user_id", "inner");
    $this->db->where("tbb.name= '" . $bahan_baku . "' AND su.user_id != '".$user_id."'");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getUmkmByBBB($bahan_baku, $user_id, &$responseCode)
  {
    $this->db->select("tu.id_umkm, tu.user_id, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',su.img_umkm_pp) as img_umkm_pp");
    $this->db->from("tbl_umkm tu");
    $this->db->join("tbl_product tp", "tu.user_id = tp.user_id", "LEFT");
    $this->db->join("sys_users su", "su.user_id=tu.user_id", "LEFT");
    $this->db->where("tp.name like '%" . $bahan_baku . "%' AND su.user_id != '".$user_id."'");
    $this->db->distinct("tu.user_id");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }


  public function createPermintaan($data, &$responseCode)
  {
    $arrData = [
      'id_penjual' => $data->id_penjual,
      'id_pembeli' => $data->id_pembeli,
      'pemesan' => $data->pemesan,
      'telp' => $data->telp,
      'alamat' => $data->alamat,
      'email' => $data->email,
      'id_bahan_baku' => $data->id_bahan_baku,
      'nama_bb' => $data->nama_bb,
      'qty' => $data->qty,
      'id_satuan_bb' => $data->id_satuan_bb,
      'create_at' => getsysdate(),
    ];
    // return $data;
    if ($this->db->insert("tbl_order_bb", $arrData)) {

      $response = [
        "status" => "success",
        "message" => 'Permintaan telah disampaikan',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dibuat',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getListPermintaanPenjual($id_penjual, &$responseCode)
  {
    $this->db->select("tob.*, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
    $this->db->from("tbl_order_bb tob");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tob.id_pembeli", "LEFT");
    $this->db->where("tob.id_penjual = '$id_penjual'");
    $this->db->order_by("tob.id_order_bb DESC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListPermintaanPembeli($id_pembeli, &$responseCode)
  {
    $this->db->select("tob.*, tu.title, ss.value_setting, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
    $this->db->from("tbl_order_bb tob");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tob.id_penjual", "LEFT");
    $this->db->join("sys_settings ss", "ss.id_setting = tob.id_satuan_bb", "LEFT");
    $this->db->where("tob.id_pembeli = '$id_pembeli' AND ss.category_setting = 'satuan_bb'");
    $this->db->order_by("tob.id_order_bb DESC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function updateStatusPermintaan($data, &$responseCode)
  {
    $arrData = [
      'is_accepted' => $data->is_accepted,
      'reject_reason' => $data->reject_reason,
      'update_at' => getsysdate(),
    ];
    // return $data;
    $this->db->where('id_order_bb', $data->id_order_bb);
    if ($this->db->update("tbl_order_bb", $arrData)) {
      $response = [
        "status" => "success",
        "message" => 'Permintaan berhasil diupdate',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal diupdate',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getUmkmById($user_id, $bb, &$responseCode)
  {
    $this->db->select("tu.id_umkm, tu.title, tu.sejarah, tku.name as kat_umkm, concat(tu.alamat, ' ', rl.nama_kelurahan, ' ', rc.nama_kecamatan, ' ', rk.nama_kabupaten, ' ', rp.nama_provinsi ) as address, su.full_name, su.no_telp, tu.telp, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm_pp/',su.img_umkm_pp) as img_umkm_pp");
    $this->db->from("tbl_umkm tu");
    $this->db->join("sys_users su", "su.user_id=tu.user_id", "LEFT");
    $this->db->join("tbl_kat_umkm AS tku", "tku.id_kat_umkm = tu.id_kat_umkm", "LEFT");
    $this->db->join("ref_provinsi AS rp", "tu.id_provinsi = rp.id_provinsi", "LEFT");
    $this->db->join("ref_kabupaten AS rk", "tu.id_kabupaten = rk.id_kabupaten", "LEFT");
    $this->db->join("ref_kecamatan AS rc", "tu.id_kecamatan = rc.id_kecamatan", "LEFT");
    $this->db->join("ref_kelurahan AS rl", "tu.id_kelurahan = rl.id_kelurahan", "LEFT");
    $this->db->where("tu.user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();

      $this->db->select("tu.id_bahan_baku, tu.id_satuan_bb, ss.value_setting");
      $this->db->from("tbl_bahan_baku tu");
      $this->db->join("sys_settings ss", "ss.id_setting = tu.id_satuan_bb", "LEFT");
      $this->db->where("tu.name= '" . $bb . "' AND ss.category_setting = 'satuan_bb'");
      $this->db->limit(1);

      $query2 = $this->db->get();
      // return $this->db->last_query();
      $bahan = $query2->row();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        'bahan' => $bahan,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
}
