<?php

// extends class Model
class Permintaan_prod_model extends CI_Model
{

  public function getListPermintaan_prod($user_id, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_permintaan_prod");
    $this->db->where("user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPermintaan_prodById($id_permintaan_prod, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_permintaan_prod");
    $this->db->where("id_permintaan_prod= " . $id_permintaan_prod);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createPermintaan_prod($data, &$responseCode)
  {
    $arrData = [
      'tanggal' => $data->tanggal,
      'user_id' => $data->user_id,
      'permintaan_prod' => $data->permintaan_prod,
      'create_at' => getsysdate(),
    ];
    if ($data->id_permintaan_prod == "") {
      if ($this->db->insert("tbl_permintaan_prod", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_permintaan_prod', $data->id_permintaan_prod);
      if ($this->db->update("tbl_permintaan_prod", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal diupdate',
        ];
        $responseCode = 404;
      }
    }
    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_permintaan_prod", $data->id);
    if ($this->db->delete("tbl_permintaan_prod")) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

}
