<?php

// extends class Model
class Media_model extends CI_Model
{

  public function getListMedia($menu, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    if ($menu == 'mentoring') {
      $this->db->where("id_kat_posts= 9");
    } else {
      $this->db->where("id_kat_posts= 3");
    }
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $yt = [];
      foreach ($rows as $row) {
        $this->db->select("tpt.id_tag, tt.name");
        $this->db->from("tbl_posts_tags AS tpt");
        $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
        $this->db->where("id_posts= " . $row['id_posts']);

        $query2 = $this->db->get();
        $rows2 = $query2->result_array();

        $row['tags'] = $rows2;
        $yt[] = $row;
      }

      if ($menu != 'mentoring') {
        $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
        $this->db->from("tbl_posts");
        $this->db->where("id_kat_posts= 1");
        $this->db->order_by('id_posts DESC');
        $this->db->limit(2);

        $query22 = $this->db->get();
        $rek = $query22->result_array();

        $yt2 = [];
        foreach ($rek as $row) {
          $this->db->select("tpt.id_tag, tt.name");
          $this->db->from("tbl_posts_tags AS tpt");
          $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
          $this->db->where("id_posts= " . $row['id_posts']);

          $query222 = $this->db->get();
          $rows222 = $query222->result_array();

          $row['tags'] = $rows222;
          $yt2[] = $row;
        }


        $this->db->select("* , concat('" . HOSTNAME . "/assets/images/banner/',img) as illustration");
        $this->db->from("tbl_banner");
        $this->db->order_by('id_banner DESC');
        $this->db->where("img != ''");
        // $this->db->limit(2);

        $query_banner = $this->db->get();
        $data_banner = $query_banner->result_array();
        $response = array(
          'status' => 'success',
          'message' => 'Data Found',
          'data' => $yt,
          'data_banner' => $data_banner,
          'rek' => $yt2
        );
      } else {
        $response = array(
          'status' => 'success',
          'message' => 'Data Found',
          'rek' => $yt
        );
      }

      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListArtikel(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts= 1");
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $yt = [];
      foreach ($rows as $row) {
        $this->db->select("tpt.id_tag, tt.name");
        $this->db->from("tbl_posts_tags AS tpt");
        $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
        $this->db->where("id_posts= " . $row['id_posts']);

        $query2 = $this->db->get();
        $rows2 = $query2->result_array();

        $row['tags'] = $rows2;
        $yt[] = $row;
      }

      $this->db->select("tpt.id_tag, tt.name");
      $this->db->from("tbl_posts_tags AS tpt");
      $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
      $this->db->join("tbl_posts AS tp", "tpt.id_posts = tp.id_posts", "LEFT");
      $this->db->where("tp.id_kat_posts= 1");
      $this->db->group_by("id_tag");

      $query3 = $this->db->get();
      $rows3 = $query3->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $yt,
        'dataTag' => $rows3,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function get_youtube($url)
  {
    $youtube = "https://www.youtube.com/oembed?url=" . $url . "&format=json";
    $curl = curl_init($youtube);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    return json_decode($return, true);
  }

  public function getListVideo(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts= 2");
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $yt = [];

      foreach ($rows as $row) {
        $parts = parse_url($row['link']);
        if(isset($parts['query'])) {
          parse_str($parts['query'], $item);
          $row['v'] = $item['v'];
          $yt[] = array_merge($row, $this->get_youtube($row['link']));
        }
      }
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $yt,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListInspirasi(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts= 3");
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $yt = [];

      foreach ($rows as $row) {
        $this->db->select("tpt.id_tag, tt.name");
        $this->db->from("tbl_posts_tags AS tpt");
        $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
        $this->db->where("id_posts= " . $row['id_posts']);

        $query2 = $this->db->get();
        $rows2 = $query2->result_array();

        $row['tags'] = $rows2;
        $yt[] = $row;
      }

      $this->db->select("tpt.id_tag, tt.name");
      $this->db->from("tbl_posts_tags AS tpt");
      $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
      $this->db->join("tbl_posts AS tp", "tpt.id_posts = tp.id_posts", "LEFT");
      $this->db->where("tp.id_kat_posts= 3");
      $this->db->group_by("id_tag");

      $query3 = $this->db->get();
      $rows3 = $query3->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $yt,
        'dataTag' => $rows3,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListMArtikel(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts= 9");
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $yt = [];
      foreach ($rows as $row) {
        $this->db->select("tpt.id_tag, tt.name");
        $this->db->from("tbl_posts_tags AS tpt");
        $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
        $this->db->where("id_posts= " . $row['id_posts']);

        $query2 = $this->db->get();
        $rows2 = $query2->result_array();

        $row['tags'] = $rows2;
        $yt[] = $row;
      }

      $this->db->select("tpt.id_tag, tt.name");
      $this->db->from("tbl_posts_tags AS tpt");
      $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
      $this->db->join("tbl_posts AS tp", "tpt.id_posts = tp.id_posts", "LEFT");
      $this->db->where("tp.id_kat_posts= 9");
      $this->db->group_by("id_tag");

      $query3 = $this->db->get();
      $rows3 = $query3->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $yt,
        'dataTag' => $rows3,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListMVideo(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts= 10");
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $yt = [];

      foreach ($rows as $row) {
        $parts = parse_url($row['link']);
        parse_str($parts['query'], $item);
        $row['v'] = $item['v'];
        $yt[] = array_merge($row, $this->get_youtube($row['link']));
      }
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $yt,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListMEbook(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/file_ebook/',img) as img_media, concat('" . HOSTNAME . "/assets/images/file_ebook/',file) as file_url");
    $this->db->from("tbl_ebook");
    $this->db->order_by('id_ebook DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListMWebinar(&$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_kat_posts= 11");
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListKnowledge($data, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    if ($data->kategori == "all" && $data->anggota == "all") {
      $this->db->where("id_kat_posts= 6 ");
    } else if ($data->anggota == "all" && $data->kategori != "all") {
      $this->db->where("id_kat_posts= 6 AND id_kat_dewan='" . $data->kategori . "' ");
    } else if ($data->kategori == "all" && $data->anggota != "all") {
      $this->db->where("id_kat_posts= 6 AND id_anggota = '" . $data->anggota . "'");
    } else {
      $this->db->where("id_kat_posts= 6 AND id_kat_dewan='" . $data->kategori . "' AND id_anggota = '" . $data->anggota . "'");
    }
    $this->db->order_by('id_posts DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      // $yt = [];
      // foreach ($rows as $row) {
      //   $this->db->select("tpt.id_tag, tt.name");
      //   $this->db->from("tbl_posts_tags AS tpt");
      //   $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
      //   $this->db->where("id_posts= " . $row['id_posts']);

      //   $query2 = $this->db->get();
      //   $rows2 = $query2->result_array();

      //   $row['tags'] = $rows2;
      //   $yt[] = $row;
      // }

      // $this->db->select("tpt.id_tag, tt.name");
      // $this->db->from("tbl_posts_tags AS tpt");
      // $this->db->join("tbl_tag AS tt", "tpt.id_tag = tt.id_tag", "LEFT");
      // $this->db->join("tbl_posts AS tp", "tpt.id_posts = tp.id_posts", "LEFT");
      // $this->db->where("tp.id_kat_posts= 1");
      // $this->db->group_by("id_tag");

      // $query3 = $this->db->get();
      // $rows3 = $query3->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        // 'dataTag' => $rows3,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListRegulasi($id_kat, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/file_regulasi/',img) as file_url");
    $this->db->from("tbl_regulasi");
    $this->db->where("id_kat_regulasi= '$id_kat'");
    $this->db->order_by('id_regulasi DESC');

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows,
        // 'dataTag' => $rows3,
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getCommentById($id, &$responseCode)
  {
    $this->db->select("tbl_komentar.*, su.full_name, concat('" . HOSTNAMEAPI . "/assets/images/img_profile/',su.img_profile) as img_profile,");
    $this->db->from("tbl_komentar");
    $this->db->join("sys_users as su", "tbl_komentar.user_id = su.user_id", "LEFT");
    $this->db->where("id_posts = '$id'");
    $this->db->order_by("tbl_komentar.create_at DESC");
    $query2 = $this->db->get();

    // return $this->db->last_query();
    if ($query2->num_rows() > 0) {
      $det['count_comment'] = $query2->num_rows();
      $komentar = ($det['count_comment'] > 0) ? $query2->result_array() : $query2->result_array();

      // $det['komentar_parent'] = array_search('0', array_column($det['komentar'], 'parent_komentar_id'));
      // $det['komentar_parent'] = array_keys(array_column($det['komentar'], 'parent_komentar_id'), 0);
      $filterBy = '0';
      $det['komentar'] = $komentar;
      $det['komentar_parent'] = array_filter($komentar, function ($var) use ($filterBy) {
        return ($var['parent_komentar_id'] == $filterBy);
      });
      $det['komentar_not_parent'] = (array) array_filter($komentar, function ($var) use ($filterBy) {
        return ($var['parent_komentar_id'] != $filterBy);
      });

      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $det
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createComment($data, &$responseCode)
  {
    // date_default_timezone_set("Asia/Jakarta");
    // $tanggal = json_decode($data->tanggal);
    // $date = new DateTime($data->tanggal);
    // $date->modify('+1 day');

    $arrData = [
      // 'tanggal' => $date->format('Y-m-d'),
      'user_id' => $data->user_id,
      'id_posts' => $data->id_posts,
      'komentar' => $data->komentar,
      'parent_komentar_id' => isset($data->parent_komentar_id) ? $data->parent_komentar_id : 0,
      // 'create_at' => getsysdate(),
    ];
    // return print_r($arrData);
    if ($this->db->insert("tbl_komentar", $arrData)) {

      $response = [
        "status" => "success",
        "message" => 'Komentar berhasil dikirim',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Komentar gagal dikirim',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getArtikelById($id_posts, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAME . "/assets/images/img_posts/',img) as img_media");
    $this->db->from("tbl_posts");
    $this->db->where("id_posts= " . $id_posts);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function log($data, &$responseCode)
  {
    $arrData = [
      'user_id' => $data->user_id,
      'id_posts' => $data->id_posts,
      'create_at' => getsysdate(),
    ];
    if ($this->db->insert("tbl_log_post", $arrData)) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dibuat',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dibuat',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getKatDewan(&$responseCode)
  {
    $this->db->select("id_kat_dewan, name");
    $this->db->from("tbl_kat_dewan");
    $this->db->order_by("name ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getAnggota(&$responseCode)
  {
    $this->db->select("id_anggota, name");
    $this->db->from("tbl_anggota");
    $this->db->order_by("name ASC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
}
