<?php

// extends class Model
class Kapasitas_prod_model extends CI_Model
{

  public function getListKapasitas_prod($user_id, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_kapasitas_prod");
    $this->db->where("user_id= " . $user_id);
    $this->db->order_by("id_kapasitas_prod DESC");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getKapasitas_prodById($id_kapasitas_prod, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_kapasitas_prod");
    $this->db->where("id_kapasitas_prod= " . $id_kapasitas_prod);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function search($data, &$responseCode)
  {
    $this->db->select("*");
    $this->db->from("tbl_kapasitas_prod");
    $this->db->where("user_id= " . $data->user_id);
    if (!empty($data->keywords)) {
      $this->db->where("tanggal", $data->keywords);
    }
    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function createKapasitas_prod($data, &$responseCode)
  {
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = json_decode($data->tanggal);
    $date = new DateTime($data->tanggal);
    // $date->modify('+1 day');

    $arrData = [
      'tanggal' => $date->format('Y-m-d'),
      'user_id' => $data->user_id,
      'kapasitas_prod' => $data->kapasitas_prod,
      // 'kapasitas_prod' => $data->tanggal,
      'create_at' => getsysdate(),
    ];
    // return $data;
    if ($data->id_kapasitas_prod == "") {
      if ($this->db->insert("tbl_kapasitas_prod", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_kapasitas_prod', $data->id_kapasitas_prod);
      if ($this->db->update("tbl_kapasitas_prod", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'Data berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'Data gagal diupdate',
        ];
        $responseCode = 404;
      }
    }
    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_kapasitas_prod", $data->id);
    if ($this->db->delete("tbl_kapasitas_prod")) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }
}
