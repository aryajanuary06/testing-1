<?php

// extends class Model
class Product_model extends CI_Model
{

  public function getPublicListProduct($offset, $search, &$responseCode)
  {
    $this->db->select("tp.* , concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product");
    $this->db->from("tbl_product tp");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "right");
    $this->db->join("sys_users as su", "su.user_id = tp.user_id", "right");
    $this->db->where("tp.name LIKE '%" . $search . "%'");
    $this->db->order_by("tp.id_product DESC");
    $this->db->limit(10, $offset);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getPublicProductById($id_product, &$responseCode)
  {
    $this->db->select("tp.id_product, tp.name, tp.price, tp.id_kat_prod, tp.desc, tkp.name AS kat_name, tu.title, rp.nama_provinsi, rk.nama_kabupaten, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product");
    $this->db->from("tbl_product tp");
    $this->db->join("tbl_kat_prod AS tkp", "tp.id_kat_prod = tkp.id_kat_prod", "LEFT");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "LEFT");
    $this->db->join("ref_provinsi AS rp", "tu.id_provinsi = rp.id_provinsi", "LEFT");
    $this->db->join("ref_kabupaten AS rk", "tu.id_kabupaten = rk.id_kabupaten", "LEFT");
    $this->db->where("id_product= " . $id_product);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function addToCart($data, &$responseCode)
  {
    $this->db->select("qty");
    $this->db->from("tbl_cart");
    $this->db->where("user_id='$data->user_id' AND id_product='$data->id_product' AND is_buy = 0");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      //update qty cart
      $dtCart = $query->row();
      $arrData = [
        'user_id' => $data->user_id,
        'id_product' => $data->id_product,
        'qty' => $data->qty + $dtCart->qty,
        'create_at' => getsysdate(),
      ];
      $this->db->where("user_id='$data->user_id' AND id_product='$data->id_product' AND is_buy = 0");
      if ($this->db->update("tbl_cart", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'gagal diupdate',
        ];
        $responseCode = 404;
      }
    } else {
      //insert to cart
      $arrData = [
        'user_id' => $data->user_id,
        'id_product' => $data->id_product,
        'qty' => $data->qty,
        'create_at' => getsysdate(),
      ];
      if ($this->db->insert("tbl_cart", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'gagal dibuat',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function getCart($user_id, &$responseCode)
  {
    $this->db->select("tc.id_product, tc.qty, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
    $this->db->from("tbl_cart tc");
    $this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
    $this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "LEFT");
    $this->db->where("user_id= " . $user_id);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getListProduct($user_id, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_product/',img) as img_product");
    $this->db->from("tbl_product");
    $this->db->where("user_id= " . $user_id);
    $this->db->order_by("id_product DESC");
    $this->db->limit(50);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }

  public function getProductById($id_product, &$responseCode)
  {
    $this->db->select("* , concat('" . HOSTNAMEAPI . "/assets/images/img_product/',img) as img_product");
    $this->db->from("tbl_product");
    $this->db->where("id_product= " . $id_product);

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->row();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }


  public function createProduct($data_img, &$responseCode)
  {
    $price = str_replace('.', '', $this->input->post('price'));
    $price = str_replace(',', '', $price);

    if (strpos($price, '-') !== false) {
      $ar_price = explode("-", $price);
      $price = $ar_price[0];
    }

    if (count($data_img) > 0) {
      if (move_uploaded_file($data_img['tmp_name'], './assets/images/img_product/' . $data_img['filename'])) {
        $source_path = FCPATH . 'assets/images/img_product/';
        $target_path = FCPATH . 'assets/images/img_product/';

        if ($data_img["size"] > 36627) {
          resizeImage($data_img['filename'], $source_path, $target_path);
        }
        $arrData = [
          'user_id' => $this->input->post('user_id'),
          'name' => $this->input->post('name'),
          'price' => $price,
          'desc' => $this->input->post('desc'),
          'id_kat_prod' => $this->input->post('id_kat_prod'),
          'img' => $data_img['filename'],
          'create_at' => getsysdate(),
        ];
      }
    } else {
      $arrData = [
        'user_id' => $this->input->post('user_id'),
        'name' => $this->input->post('name'),
        'price' => $price,
        'desc' => $this->input->post('desc'),
        'id_kat_prod' => $this->input->post('id_kat_prod'),
        'create_at' => getsysdate(),
      ];
    }

    if ($this->input->post('id_product') == "") {
      if ($this->db->insert("tbl_product", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'product berhasil dibuat',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'product gagal dibuat',
        ];
        $responseCode = 404;
      }
    } else {
      $this->db->where('id_product', $this->input->post('id_product'));
      if ($this->db->update("tbl_product", $arrData)) {

        $response = [
          "status" => "success",
          "message" => 'product berhasil diupdate',
        ];
        $responseCode = 201;
      } else {
        $response = [
          "status" => "error",
          "message" => 'product gagal diupdate',
        ];
        $responseCode = 404;
      }
    }

    return $response;
  }

  public function hapus($data, &$responseCode)
  {
    $this->db->where("id_product", $data->id);
    if ($this->db->delete("tbl_product")) {
      $response = [
        "status" => "success",
        "message" => 'Data berhasil dihapus',
      ];
      $responseCode = 201;
    } else {
      $response = [
        "status" => "error",
        "message" => 'Data gagal dihapus',
      ];
      $responseCode = 404;
    }
    return $response;
  }

  public function getKatProduct(&$responseCode)
  {
    $this->db->select("id_kat_prod, name");
    $this->db->from("tbl_kat_prod");

    $query = $this->db->get();
    // return $this->db->last_query();
    if ($query->num_rows() > 0) {
      $rows = $query->result_array();
      $response = array(
        'status' => 'success',
        'message' => 'Data Found',
        'data' => $rows
      );
      $responseCode = 200;
      return $response;
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data Not Found'
      );
      $responseCode = 200;
      return $response;
    }
  }
}
