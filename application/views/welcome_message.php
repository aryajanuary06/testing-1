<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/Welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html> -->

<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td style="padding:12px 0"></td>
		</tr>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="600" align="center" style="border-collapse:collapse;background-color:#ffffff;margin:0 auto">
					<tbody>
						<tr>
							<td style="padding:36px 20px 18px">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:15px">
									<tbody>
										<tr>
											<td style="color:#4e4e4e;line-height:25px">
												<div style="font-size:16px;color:rgba(49,53,59,0.96);line-height:1.43">
													Hai <strong><?= $penerima['penerima'] ?>,</strong>
												</div>
												<div style="color:rgba(49,53,59,0.96);font-size:20px;font-weight:bold;margin:16px 0 0">
													Terimakasih sudah berbelanja dan mendukung para penjual di UMKM Tohaga.
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>

						<?php
						// print_r(array_keys($rows));
						// print_r($rows['umkmffff']);
						$total = 0;
						foreach ($umkm as $toko) {
						?>
							<tr>
								<td style="padding:0 20px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
										<tbody>
											<!-- <tr>
											<td style="padding:0 0 10px">
												<div style="color:#212121;font-size:14px">
													No.<span class="il">Invoice</span>: <a href="http://fapp1.tokopedia.com/WSGQHPRTIW?id=16114=IRlUVgZSAwlXHgEDVAgJUQMJVE4=TQRYB0MCQkwHVlNWVlAkAVoFW1RNAV5eGAkPUQYLXQAGVQNWB1MAA1MLTgsWERRcGEtUWRMSAB0QV1IOQ1wAW1VLVQlaTGdmIWl6MzAxLTEIDVZEFVA=&amp;fl=ChEQFkReHRcXDVpcFF1dCFIXB11ZSl8IQQxZVgMWQg8=&amp;ext=aWQ9NjMwOTAwMjM5JmFtcDtwZGY9SW52b2ljZS0xNjMzMTU0LTQyNTkzNDYtMjAyMDExMTEwOTU2MTEtYjI5dmIyOXZiMjgzLnBkZiZhbXA7dXRtX3NvdXJjZT1sUFZhUWk2TiZhbXA7dXRtX21lZGl1bT1qTmF5clF3TyZhbXA7dXRtX2NhbXBhaWduPUNPX1RYLUJVWS1TVEktRklOXzBfQk9fMCZhbXA7dXRtX2NvbnRlbnQ9SU5W" style="color:#03ac0e;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://fapp1.tokopedia.com/WSGQHPRTIW?id%3D16114%3DIRlUVgZSAwlXHgEDVAgJUQMJVE4%3DTQRYB0MCQkwHVlNWVlAkAVoFW1RNAV5eGAkPUQYLXQAGVQNWB1MAA1MLTgsWERRcGEtUWRMSAB0QV1IOQ1wAW1VLVQlaTGdmIWl6MzAxLTEIDVZEFVA%3D%26fl%3DChEQFkReHRcXDVpcFF1dCFIXB11ZSl8IQQxZVgMWQg8%3D%26ext%3DaWQ9NjMwOTAwMjM5JmFtcDtwZGY9SW52b2ljZS0xNjMzMTU0LTQyNTkzNDYtMjAyMDExMTEwOTU2MTEtYjI5dmIyOXZiMjgzLnBkZiZhbXA7dXRtX3NvdXJjZT1sUFZhUWk2TiZhbXA7dXRtX21lZGl1bT1qTmF5clF3TyZhbXA7dXRtX2NhbXBhaWduPUNPX1RYLUJVWS1TVEktRklOXzBfQk9fMCZhbXA7dXRtX2NvbnRlbnQ9SU5W&amp;source=gmail&amp;ust=1640834458553000&amp;usg=AOvVaw2ZdffcyjgxdHx1EM9NEmKR">INV/20201111/XX/XI/672460132</a>
												</div>
											</td>
										</tr> -->
											<tr>
												<td>
													<div style="color:#212121;font-size:14px">
														Toko: <?= $toko ?>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<?php
							$subtotal = 0;
							foreach ($rows[$toko] as $row) {
								$subtotal += $row['price'] * $row['qty']

							?>

								<tr>
									<td style="padding:12px 20px 10px">
										<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
											<tbody>
												<tr>
													<td valign="top" style="border-bottom:1px solid #e4eaf3;padding:12px 0 0">
														<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96);font-size:12px">

															<tbody>
																<tr>
																	<td valign="top" width="64" style="padding:0 0 16px 0">
																		<img src="<?= $row['img_product'] ?>" width="64" height="64" style="border-radius:8px" class="CToWUd">
																	</td>
																	<td valign="top" style="padding:0 0 16px 16px">
																		<div style="margin:0 0 4px;line-height:16px"><?= $row['email'].$row['name'] ?></div>
																		<p style="font-weight:bold;margin:12px 0 0"><?= $row['qty'] ?> x
																			<span style="font-weight:bold;font-size:12px;color:#fa591d">Rp <?= number_format($row['price'], 0, ',', '.') ?></span>
																		</p>
																	</td>
																	<td valign="top" align="right" style="padding:0 0 12px">
																		<span style="font-weight:bold;font-size:12px;color:#fa591d">Rp <?= number_format($row['qty'] * $row['price'], 0, ',', '.') ?></span>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>

							<?php
								// print_r($row);
							}
							?>

							<tr>
								<td style="padding:10px 20px 24px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;border-bottom:1px solid #e4eaf3">
										<tbody>
											<tr>
												<td style="font-size:12px;color:rgba(49,53,59,0.68);padding:0 0 8px">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
														<tbody>
															<tr>
																<td style="font-size:12px;padding:0 0 12px">Total Harga Barang</td>
																<td style="font-size:12px;padding:0 0 12px; text-align: right;">Rp <?= number_format($subtotal, 0, ',', '.') ?></td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						<?php
							$total += $subtotal;
						}
						?>

						<tr>
							<td style="padding:0 20px 24px">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:14px">
									<tbody>
										<tr>
											<td width="50%">
												<div style="color:rgba(49,53,59,0.96);font-weight:bold">Total Belanja</div>
											</td>
											<td width="50%" align="right">
												<div style="font-weight:bold;color:#fa591d">Rp <?= number_format($total, 0, ',', '.') ?></div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:0 20px 24px">
								<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f3f4f5;border-radius:12px">
									<tbody>
										<tr>
											<td style="padding:16px 24px">
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:14px">
													<tbody>
														<tr>
															<td valign="top" width="50%">
																<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96)">
																	<tbody>
																		<tr>
																			<td style="color:rgba(49,53,59,0.68);font-weight:bold;padding:0 0 16px">
																				Penerima
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<div style="font-size:14px;font-weight:bold;margin:0 0 8px">
																				<?= $penerima["penerima"] ?>
																				</div>
																				<div style="font-size:14px;color:rgba(49,53,59,0.68);line-height:20px">
																				<?= $penerima["alamat"] ?>

																					<br>
																					Telp: <?= $penerima["telp"] ?>

																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:24px 20px">
								<div style="font-size:12px;color:rgba(49,53,59,0.96);line-height:1.5">
									E-mail ini dibuat secara otomatis, mohon tidak membalas.
									<br>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>