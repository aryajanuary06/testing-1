<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require APPPATH . 'libraries/phpmailer/src/Exception.php';
require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
require APPPATH . 'libraries/phpmailer/src/SMTP.php';

function sendEmail($destinationEmail, $pass)
{

	$from = "admin@umkmtohaga.com";
	$to = $destinationEmail;
	$subject = "Reset Password";
	$message = "Halo " . $destinationEmail . "<BR><br>";
	$message .= "Password baru kamu adalah " . $pass . "<br>";
	$message .= "Silahkan login dengan password ini.<br><br>";
	$message .= "Terima kasih,";

	$separator = md5(time());
	$header = "From: UMKM Tohaga <admin@umkmtohaga.com>\r\n";
	$header .= "Reply-To:" . $from . "\r\n" .
		"X-Mailer: PHP/" . phpversion();
	$header .= "Organization: UMKM Tohaga\r\n";
	$header .= "Return-Path: UMKM Tohaga <admin@umkmtohaga.com>\r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-Type: text/html; charset: utf8; boundary=\"" . $separator . "\"";
	$header .= "Content-Transfer-Encoding: 7bit";

	mail($to, $subject, $message, $header);
	// echo "Pesan email sudah terkirim.";
}

function _group_by($array, $key)
{
	$return = array();
	foreach ($array as $val) {
		$return[$val[$key]][] = $val;
	}
	return $return;
}

function sendEmailInvoice($destinationEmail, $order_id, $penerima)
{

	$from = "admin@umkmtohaga.com";
	$to = $destinationEmail;
	$subject = "Invoice";
	// $message = "Halo " . $destinationEmail . "<BR><br>";
	// $message .= "Terimakasih sudah berbelanja dan mendukung para penjual di UMKM Tohaga.<br><br>";

	$ci = &get_instance();

	//load databse library
	$ci->load->database();

	$ci->db->select("su.email, tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
	$ci->db->from("tbl_cart tc");
	$ci->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
	$ci->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
	$ci->db->join("sys_users AS su", "tu.user_id = su.user_id", "LEFT");
	$ci->db->where("tc.id_order= '$order_id' AND tc.is_buy = 1 AND tc.is_ceklis = 1");

	$query = $ci->db->get();
	$list = $query->result_array();

	$rows = _group_by($list, 'title');
	$umkm = array_keys($rows);


	$message = '<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td style="padding:12px 0"></td>
		</tr>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="0" border="0" width="600" align="center" style="border-collapse:collapse;background-color:#ffffff;margin:0 auto">
					<tbody>
						<tr>
							<td style="padding:36px 20px 18px">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:15px">
									<tbody>
										<tr>
											<td style="color:#4e4e4e;line-height:25px">
												<div style="font-size:16px;color:rgba(49,53,59,0.96);line-height:1.43">
													Hai <strong>' . $penerima["penerima"] . ',</strong>
												</div>
												<div style="color:rgba(49,53,59,0.96);font-size:20px;font-weight:bold;margin:16px 0 0">
													Terimakasih sudah berbelanja dan mendukung para penjual di UMKM Tohaga.
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>

						';
	$total = 0;
	foreach ($umkm as $toko) {
		$message .= '
							<tr>
								<td style="padding:0 20px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
										<tbody>
											<!-- <tr>
											<td style="padding:0 0 10px">
												<div style="color:#212121;font-size:14px">
													No.<span class="il">Invoice</span>: <a href="http://fapp1.tokopedia.com/WSGQHPRTIW?id=16114=IRlUVgZSAwlXHgEDVAgJUQMJVE4=TQRYB0MCQkwHVlNWVlAkAVoFW1RNAV5eGAkPUQYLXQAGVQNWB1MAA1MLTgsWERRcGEtUWRMSAB0QV1IOQ1wAW1VLVQlaTGdmIWl6MzAxLTEIDVZEFVA=&amp;fl=ChEQFkReHRcXDVpcFF1dCFIXB11ZSl8IQQxZVgMWQg8=&amp;ext=aWQ9NjMwOTAwMjM5JmFtcDtwZGY9SW52b2ljZS0xNjMzMTU0LTQyNTkzNDYtMjAyMDExMTEwOTU2MTEtYjI5dmIyOXZiMjgzLnBkZiZhbXA7dXRtX3NvdXJjZT1sUFZhUWk2TiZhbXA7dXRtX21lZGl1bT1qTmF5clF3TyZhbXA7dXRtX2NhbXBhaWduPUNPX1RYLUJVWS1TVEktRklOXzBfQk9fMCZhbXA7dXRtX2NvbnRlbnQ9SU5W" style="color:#03ac0e;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://fapp1.tokopedia.com/WSGQHPRTIW?id%3D16114%3DIRlUVgZSAwlXHgEDVAgJUQMJVE4%3DTQRYB0MCQkwHVlNWVlAkAVoFW1RNAV5eGAkPUQYLXQAGVQNWB1MAA1MLTgsWERRcGEtUWRMSAB0QV1IOQ1wAW1VLVQlaTGdmIWl6MzAxLTEIDVZEFVA%3D%26fl%3DChEQFkReHRcXDVpcFF1dCFIXB11ZSl8IQQxZVgMWQg8%3D%26ext%3DaWQ9NjMwOTAwMjM5JmFtcDtwZGY9SW52b2ljZS0xNjMzMTU0LTQyNTkzNDYtMjAyMDExMTEwOTU2MTEtYjI5dmIyOXZiMjgzLnBkZiZhbXA7dXRtX3NvdXJjZT1sUFZhUWk2TiZhbXA7dXRtX21lZGl1bT1qTmF5clF3TyZhbXA7dXRtX2NhbXBhaWduPUNPX1RYLUJVWS1TVEktRklOXzBfQk9fMCZhbXA7dXRtX2NvbnRlbnQ9SU5W&amp;source=gmail&amp;ust=1640834458553000&amp;usg=AOvVaw2ZdffcyjgxdHx1EM9NEmKR">INV/20201111/XX/XI/672460132</a>
												</div>
											</td>
										</tr> -->
											<tr>
												<td>
													<div style="color:#212121;font-size:14px">
														Toko: ' . $toko . '
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							';
		$subtotal = 0;
		$email_penjual = "yanataryana545@gmail.com";
		$message_penjual = "";
		foreach ($rows[$toko] as $row) {
			$email_penjual = $row["email"];
			$subtotal += $row['price'] * $row['qty'];

			$message .= '

								<tr>
									<td style="padding:12px 20px 10px">
										<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
											<tbody>
												<tr>
													<td valign="top" style="border-bottom:1px solid #e4eaf3;padding:12px 0 0">
														<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96);font-size:12px">

															<tbody>
																<tr>
																	<td valign="top" width="64" style="padding:0 0 16px 0">
																		<img src="' . str_replace(" ", "%20", $row['img_product']) . '" width="64" height="64" style="border-radius:8px" class="CToWUd">
																	</td>
																	<td valign="top" style="padding:0 0 16px 16px">
																		<div style="margin:0 0 4px;line-height:16px">' . $row['name'] . '</div>
																		<p style="font-weight:bold;margin:12px 0 0">' . $row['qty'] . ' x
																			<span style="font-weight:bold;font-size:12px;color:#fa591d">Rp ' . number_format($row['price'], 0, ',', '.') . '</span>
																		</p>
																	</td>
																	<td valign="top" align="right" style="padding:0 0 12px">
																		<span style="font-weight:bold;font-size:12px;color:#fa591d">Rp ' . number_format($row['qty'] * $row['price'], 0, ',', '.') . '</span>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>

							';
			$message_penjual .= '

							<tr>
								<td style="padding:12px 20px 10px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
										<tbody>
											<tr>
												<td valign="top" style="border-bottom:1px solid #e4eaf3;padding:12px 0 0">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96);font-size:12px">

														<tbody>
															<tr>
																<td valign="top" width="64" style="padding:0 0 16px 0">
																	<img src="' . str_replace(" ", "%20", $row['img_product']) . '" width="64" height="64" style="border-radius:8px" class="CToWUd">
																</td>
																<td valign="top" style="padding:0 0 16px 16px">
																	<div style="margin:0 0 4px;line-height:16px">' . $row['name'] . '</div>
																	<p style="font-weight:bold;margin:12px 0 0">' . $row['qty'] . ' x
																		<span style="font-weight:bold;font-size:12px;color:#fa591d">Rp ' . number_format($row['price'], 0, ',', '.') . '</span>
																	</p>
																</td>
																<td valign="top" align="right" style="padding:0 0 12px">
																	<span style="font-weight:bold;font-size:12px;color:#fa591d">Rp ' . number_format($row['qty'] * $row['price'], 0, ',', '.') . '</span>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>

						';
			// print_r($row);
		}
		$message .= '

							<tr>
								<td style="padding:10px 20px 24px">
									<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;border-bottom:1px solid #e4eaf3">
										<tbody>
											<tr>
												<td style="font-size:12px;color:rgba(49,53,59,0.68);padding:0 0 8px">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
														<tbody>
															<tr>
																<td style="font-size:14px;font-weight:bold;padding:0 0 12px">Total Harga Barang</td>
																<td style="font-size:14px;font-weight:bold;padding:0 0 12px; text-align: right;">Rp ' . number_format($subtotal, 0, ',', '.') . '</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						';
		$message_penjual .= '

						<tr>
							<td style="padding:10px 20px 24px">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;border-bottom:1px solid #e4eaf3">
									<tbody>
										<tr>
											<td style="font-size:12px;color:rgba(49,53,59,0.68);padding:0 0 8px">
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
													<tbody>
														<tr>
															<td style="font-size:14px;font-weight:bold;padding:0 0 12px">Total Harga Barang</td>
															<td style="font-size:14px;font-weight:bold;padding:0 0 12px; text-align: right;">Rp ' . number_format($subtotal, 0, ',', '.') . '</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					';
		$message_penjual .= '

					<tr>
						<td style="padding:0 20px 24px">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f3f4f5;border-radius:12px">
								<tbody>
									<tr>
										<td style="padding:16px 24px">
											<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:14px">
												<tbody>
													<tr>
														<td valign="top" width="50%">
															<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96)">
																<tbody>
																	<tr>
																		<td style="color:rgba(49,53,59,0.68);font-weight:bold;padding:0 0 16px">
																			Penerima
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<div style="font-size:14px;font-weight:bold;margin:0 0 8px">
																			' . $penerima["penerima"] . '
																			</div>
																			<div style="font-size:14px;color:rgba(49,53,59,0.68);line-height:20px">
																			' . $penerima["alamat"] . '
					
																				<br>
																				Telp: ' . $penerima["telp"] . '
					
																			</div>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding:24px 20px">
							<div style="font-size:12px;color:rgba(49,53,59,0.96);line-height:1.5">
								E-mail ini dibuat secara otomatis, mohon tidak membalas.
								<br>
							</div>
						</td>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>';
		sendEmailtoPenjual($email_penjual, $message_penjual);
		$total += $subtotal;
	}
	$message .= '

						<tr>
							<td style="padding:0 20px 24px">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:14px">
									<tbody>
										<tr>
											<td width="50%">
												<div style="color:rgba(49,53,59,0.96);font-weight:bold">Total Belanja</div>
											</td>
											<td width="50%" align="right">
												<div style="font-weight:bold;color:#fa591d">Rp ' . number_format($total, 0, ',', '.') . '</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:0 20px 24px">
								<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f3f4f5;border-radius:12px">
									<tbody>
										<tr>
											<td style="padding:16px 24px">
												<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:14px">
													<tbody>
														<tr>
															<td valign="top" width="50%">
																<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96)">
																	<tbody>
																		<tr>
																			<td style="color:rgba(49,53,59,0.68);font-weight:bold;padding:0 0 16px">
																				Penerima
																			</td>
																		</tr>
																		<tr>
																			<td>
																				<div style="font-size:14px;font-weight:bold;margin:0 0 8px">
																				' . $penerima["penerima"] . '
																				</div>
																				<div style="font-size:14px;color:rgba(49,53,59,0.68);line-height:20px">
																				' . $penerima["alamat"] . '

																					<br>
																					Telp: ' . $penerima["telp"] . '

																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding:24px 20px">
								<div style="font-size:12px;color:rgba(49,53,59,0.96);line-height:1.5">
									E-mail ini dibuat secara otomatis, mohon tidak membalas.
									<br>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>  ';



	$separator = md5(time());
	$header = "From: UMKM Tohaga <admin@umkmtohaga.com>\r\n";
	$header .= "Reply-To:" . $from . "\r\n" .
		"X-Mailer: PHP/" . phpversion();
	$header .= "Organization: UMKM Tohaga\r\n";
	$header .= "Return-Path: UMKM Tohaga <admin@umkmtohaga.com>\r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-Type: text/html; charset: utf8; boundary=\"" . $separator . "\"";
	$header .= "Content-Transfer-Encoding: 7bit";

	return mail($to, $subject, $message, $header);
	// echo "Pesan email sudah terkirim.";
}

function sendEmailtoPenjual($destinationEmail, $message)
{

	$from = "admin@umkmtohaga.com";
	$to = $destinationEmail;
	$subject = "Order Masuk";

	$separator = md5(time());
	$header = "From: UMKM Tohaga <admin@umkmtohaga.com>\r\n";
	$header .= "Reply-To:" . $from . "\r\n" .
		"X-Mailer: PHP/" . phpversion();
	$header .= "Organization: UMKM Tohaga\r\n";
	$header .= "Return-Path: UMKM Tohaga <admin@umkmtohaga.com>\r\n";
	$header .= "MIME-Version: 1.0\r\n";
	$header .= "Content-Type: text/html; charset: utf8; boundary=\"" . $separator . "\"";
	$header .= "Content-Transfer-Encoding: 7bit";

	return mail($to, $subject, $message, $header);
	// echo "Pesan email sudah terkirim.";
}

function send_email_notif($recipient, $subject, $message, $attachment_file = '', $attachement_name = '')
{

	$email = new PHPMailer();

	$email->SMTPDebug = 0;                                 // Enable verbose debug output
	$email->isSMTP();                                      // Set mailer to use SMTP
	$email->Host = 'srv102.niagahoster.com';  // Specify main and backup SMTP servers
	$email->SMTPAuth = true;                               // Enable SMTP authentication
	$email->Username = 'esign.sjs@sinarjernihsuksesindo.com';                 // SMTP username
	$email->Password = 'Mu5t1kaR@tuJkt!';                           // SMTP password
	$email->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$email->Port = 587;


	$default_sender_name = get_sys_setting("023");
	$default_sender_alias_name = get_sys_setting("027");
	//  $email->SetFrom($default_sender_name, $default_sender_alias_name); //Name is optional
	$email->SetFrom($default_sender_name); //Name is optional
	$email->Subject   = $subject; // 'Dokumen Kontrak - '.$nm_pegawai;
	//Send HTML or Plain Text email
	$email->isHTML(true);
	$separator = md5(time());
	$headers  = "MIME-Version: 1.0";
	$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
	$headers .= "Content-Transfer-Encoding: 7bit";
	$email->AddCustomHeader($headers);
	//  $email->AddCustomHeader('Content-Type: multipart/mixed'); 
	//  $email->AddCustomHeader("X-Mailer: PHP/" . phpversion()); 
	$email->addReplyTo($default_sender_name, "PKWT Online SJS");

	$email->Body = $message;

	$email->AddAddress($recipient);
	$email->AddAttachment($attachment_file, $attachement_name);

	return $email->Send();
}


function send_email_test()
{

	$recipient = 'jaja.suparja@gmail.com';
	$subject = " Email pkwt ";
	$message = "Hi Jajas,<br><br>Selamat karena sudah menjadi bagian dari keluarga besar SJS<br><br>Silahkan melakukan penandatangan Kontrak di link berikut<br>Link akan expired setelah 14 hari";
	$attachment_file = 'docs/pdf/PKWT_SJB001010520_I.pdf';
	$email = new PHPMailer();
	$default_sender_name = get_sys_setting("023");
	$default_sender_alias_name = get_sys_setting("027");
	$email->SetFrom($default_sender_name);
	$email->Subject   = $subject;

	//Send HTML or Plain Text email
	$email->isHTML(true);
	$separator = md5(time());
	$headers  = "MIME-Version: 1.0";
	$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
	$headers .= "Content-Transfer-Encoding: 7bit";
	$email->AddCustomHeader($headers);
	//  $email->AddCustomHeader('Content-Type: multipart/mixed'); 
	//  $email->AddCustomHeader("X-Mailer: PHP/" . phpversion()); 
	$email->addReplyTo($default_sender_name, "PKWT Online SJS");

	$email->Body = $message;

	$email->AddAddress($recipient);
	$email->AddAttachment($attachment_file);

	return $email->Send();
}


function send_email_notif_check($recipient, $subject, $message)
{

	$email = new PHPMailer();

	$email->SMTPDebug = 0;                                 // Enable verbose debug output
	$email->isSMTP();                                      // Set mailer to use SMTP
	$email->Host = 'srv102.niagahoster.com';  // Specify main and backup SMTP servers
	$email->SMTPAuth = true;                               // Enable SMTP authentication
	$email->Username = 'esign.sjs@sinarjernihsuksesindo.com';                 // SMTP username
	$email->Password = 'Mu5t1kaR@tuJkt!';                           // SMTP password
	$email->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$email->Port = 587;

	$default_sender_name = get_sys_setting("023");
	$default_sender_alias_name = get_sys_setting("027");
	//  $email->SetFrom($default_sender_name, $default_sender_alias_name); //Name is optional
	$email->SetFrom($default_sender_name); //Name is optional
	$email->Subject   = $subject; // 'Dokumen Kontrak - '.$nm_pegawai;
	//Send HTML or Plain Text email
	$email->isHTML(true);
	$email->AddCustomHeader('MIME-Version: 1.0" . "\r\n');
	$email->AddCustomHeader('Content-type: text/html; charset=iso-8859-1" . "\r\n');
	$email->AddCustomHeader("X-Mailer: PHP/" . phpversion());
	$email->addReplyTo($default_sender_name, "PKWT Online SJS");

	$email->Body = $message;

	$email->AddAddress($recipient);

	return $email->Send();
}
