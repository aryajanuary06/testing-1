<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->db->select("su.email, tc.id_cart, tc.id_product, tc.qty, tc.is_ceklis, tp.name, tp.price, tu.title, concat('" . HOSTNAMEAPI . "/assets/images/img_product/',tp.img) as img_product, concat('" . HOSTNAMEAPI . "/assets/images/img_umkm/',tu.img) as img_umkm");
		$this->db->from("tbl_cart tc");
		$this->db->join("tbl_product AS tp", "tc.id_product = tp.id_product", "LEFT");
		$this->db->join("tbl_umkm AS tu", "tu.user_id = tp.user_id", "INNER");
		$this->db->join("sys_users AS su", "tu.user_id = su.user_id", "LEFT");
		$this->db->where("tc.id_order= '1' AND is_buy = 1 AND tc.is_ceklis = 1");

		$query = $this->db->get();
		$rows = $query->result_array();
    // echo $this->db->last_query();

		$list = $this->_group_by($rows, 'title');
		$umkm = array_keys($list);
		print_r($list);
		$arrDataOrder = [
      'penerima' => 'penerimapenerimapenerimapenerimapenerima',
      'telp' => '06895486984956',
      'alamat' => 'alamatalamatalamatalamatalamat',
      'email' => 'email',
    ];

		$data = array(
			'rows' => $list,
			'umkm' => $umkm,
			'penerima' => $arrDataOrder,
		);
		$this->load->view('welcome_message', $data);
	}

	function _group_by($array, $key)
	{
		$return = array();
		foreach ($array as $val) {
			$return[$val[$key]][] = $val;
		}
		return $return;
	}
}
